#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

import unittest
import LandmarkMatcher

class TestMatch(unittest.TestCase):

    def setUp(self):
        self.lm = LandmarkMatcher.LandmarkMatcher()

    def testFullMatch(self):
        self.assertEquals(self.lm.match("rideau centre"), "RIDEAU CENTRE")

    def testPartialMatchRideau(self):
        self.assertEquals(self.lm.match("rideau"), "RIDEAU TRANSIT MALL")

    def testPartialMatchBayshore(self):
        self.assertEquals(self.lm.match("bayshore"), "BAYSHORE STATION")

if __name__ == '__main__':
    unittest.main()
