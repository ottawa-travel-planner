#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

import unittest
import Itinerary

# doesn't belong here
import ShortFormatter

class TestItinGrab(unittest.TestCase):

    def setUp(self):
        fp = open(self.filename, "r")
        html = fp.read()
        fp.close()

        itinp = Itinerary.ItineraryParser()
        itinp.feed(html)
        itinp.close()

        self.entries = itinp.entries

    def checkAllRecognized(self):
        for ie in self.entries:
            self.assertNotEquals(ie.type, Itinerary.TYPE_UNKNOWN)


class TestChampagneItin(TestItinGrab):

    filename = "grabs/champagne-itin.html"

    def testNumEntries(self):
        self.assertEquals(len(self.entries), 4)

    def testBasic(self):
        self.checkAllRecognized()

class TestLongItin(TestItinGrab):

    filename = "grabs/long-itin.html"

    def testNumEntries(self):
        self.assertEquals(len(self.entries), 9)

    def testBasic(self):
        self.checkAllRecognized()

    def testHop3(self):
        ie = self.entries[2];
        self.assertEquals(ie.type, Itinerary.TYPE_TAKE_BUS)
        self.assertEquals(ie.busStop, "1635")
        self.assertEquals(ie.duration, None)
        self.assertEquals(ie.startTime, '11:10 AM')
        self.assertEquals(ie.endTime, '11:20 AM')
        self.assertEquals(ie.route, '116')
        self.assertEquals(ie.direction, 'Baseline')
        self.assertEquals(ie.destination, 'MERIVALE / COLONNADE')

class TestProblemsIHad(unittest.TestCase):

    def testTakeBusWithNote(self):
        text = """At 1:35 PM, take Bus route 97 direction Bayshore
                  and get off at station HURDMAN STOP / ARRET 2A.
                  Arrive at 1:40 PM.


                   Note"""
        match = Itinerary._take_bus_rx.search(text)
        self.assert_(match)

    def testJustWalkDummy(self):
        # e.g. 916 meadowlands to 918 meadowlands
        text = """At 1:45 PM, walk to 918 MEADOWLANDS.
                  Arrive at 1:46 PM (1 min.)."""

        match = Itinerary._walk_to_dest_rx.search(text)
        self.assert_(match)

if __name__ == '__main__':
    unittest.main()
