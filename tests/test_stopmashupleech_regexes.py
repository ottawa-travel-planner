# vi: set fileencoding=latin1 softtabstop=4 shiftwidth=4 tabstop=8 expandtab:
#
# The above line tells Python to use Latin-1, which seems to be right for the
# accented characters coming back from the travel planner.

import unittest

import sys
sys.path.append("ocdata")
import stopMashupLeech

class TestAddrRegex(unittest.TestCase):
    def testRiverside2865(self):
        text = "RIVERSIDE / AD. 2865"
        match = stopMashupLeech._addr_rx.match(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("street1"), "RIVERSIDE")
        self.assertEquals(match.group("number"), "2865")

class TestIntersectionRegex(unittest.TestCase):
    def testBasic(self):
        text = "BANK / RIVERSIDE"
        match = stopMashupLeech._intersection_rx.match(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("street1"), "BANK")
        self.assertEquals(match.group("street2"), "RIVERSIDE")

class TestStationStopRegex(unittest.TestCase):
    def testIgnoreStationStop(self):
        text = r"HERON STOP / ARR�T 4A"
        match = stopMashupLeech._station_stop_rx.match(text)
        self.assertNotEquals(match, None)
