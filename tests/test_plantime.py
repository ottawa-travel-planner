#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

import unittest
import PlanTime


class TestPlanTime(unittest.TestCase):

    def testPMParams(self):
        # 1118005621 -> Sun Jun  5 17:07:01 EDT 2005
        t = 1118005621
        pt = PlanTime.PlanTime(t, PlanTime.MUST_LEAVE_AFTER)
        self.assertEquals(pt.toPlannerParams(),
                          (("dateSelect", "2005-06-05"),
                           ("day", "2005-06-05"),
                           ("visibleDay", "June 5"),
                           ("requestCode", 3),
                           ("time", "05:07"),
                           ("timePeriod", "pm"),
                           ("ok.x", 29),
                           ("ok.y", 21)))

if __name__ == '__main__':
    unittest.main()
