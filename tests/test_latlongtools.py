# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

import unittest
import LatLongTools
from BusStopMashup import StopLocation

class TestFindCorners(unittest.TestCase):
    def testSimple(self):
        center = StopLocation(45, -75)
        points = { 'sw1': (44.1, -75.1), 'sw2': (44.1, -75.05),
                   'sw3': (44.1, -75.2),
                   'nw1': (45.1, -76),
                   'ne1': (45.1, -74),
                   'se1': (44, -74) }
        corners = LatLongTools.findCorners(center,
                              [(StopLocation(points[k][0], points[k][1]), k)
                                  for k in points.iterkeys()])

        self.assertEquals(4, len(corners))
        self.assertEquals(1, len(corners[LatLongTools.NORTHWEST]))
        self.assertEquals(1, len(corners[LatLongTools.NORTHEAST]))
        self.assertEquals(3, len(corners[LatLongTools.SOUTHWEST]))
        self.assertEquals(1, len(corners[LatLongTools.SOUTHEAST]))

        self.assertEquals("nw1", corners[LatLongTools.NORTHWEST][0])
        self.assertEquals("ne1", corners[LatLongTools.NORTHEAST][0])
        self.assertEquals("se1", corners[LatLongTools.SOUTHEAST][0])

        sw = corners[LatLongTools.SOUTHWEST]
        self.assertEquals("sw3", sw[0])
        self.assertEquals("sw1", sw[1])
        self.assertEquals("sw2", sw[2])

if __name__ == '__main__':
    unittest.main()
