#!/usr/bin/python
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

import unittest
from RectTools import *

class testRectangle(unittest.TestCase):
    def testEq(self):
        r1 = Rectangle(0, 0, 2, 2)
        r2 = Rectangle(0, 0, 2, 2)
        self.assertEquals(r1, r2)

class TestIntersection(unittest.TestCase):
    def testNonIntersectingEntirely(self):
        r1 = Rectangle(0, 0, 2, 2)
        r2 = Rectangle(5, 5, 3, 3)
        inter = intersection(r1, r2)
        self.assertEquals(inter, None)

    def testNonIntersectingX(self):
        r1 = Rectangle(0, 0, 2, 2)
        r2 = Rectangle(3, 0, 3, 3)
        inter = intersection(r1, r2)
        self.assertEquals(inter, None)

    def testNonIntersectingY(self):
        r1 = Rectangle(0, 0, 2, 2)
        r2 = Rectangle(0, 3, 3, 3)
        inter = intersection(r1, r2)
        self.assertEquals(inter, None)

    def testIdentical(self):
        r1 = Rectangle(0, 0, 2, 2)
        inter = intersection(r1, r1)
        self.assertEquals(r1, inter)

    def testBorder(self):
        r1 = Rectangle(0, 0, 2, 2)
        r2 = Rectangle(2, 2, 2, 2)
        inter = intersection(r1, r2)
        self.assertEquals(inter, None)

    def testR1InsideR2(self):
        r1 = Rectangle(2, 2, 2, 2)
        r2 = Rectangle(1, 1, 3, 3)
        inter = intersection(r1, r2)
        self.assertEquals(r1, inter)

    def testR2InsideR1(self):
        r2 = Rectangle(2, 2, 2, 2)
        r1 = Rectangle(1, 1, 3, 3)
        inter = intersection(r1, r2)
        self.assertEquals(r2, inter)

    def testR1OverlapLeftEntirely(self):
        r1 = Rectangle(1, 1, 2, 2)
        r2 = Rectangle(2, 1, 2, 2)
        inter = intersection(r1, r2)
        self.assertEquals(Rectangle(2, 1, 1, 2), inter)

    def testR1OverlapR2TopLeft(self):
        r1 = Rectangle(1, 1, 2, 2)
        r2 = Rectangle(2, 1, 2, 3)
        inter = intersection(r1, r2)
        self.assertEquals(Rectangle(2, 1, 1, 2), inter)

    def testR1OverlapR2BottomRight(self):
        r1 = Rectangle(6, 2, 4, 4)
        r2 = Rectangle(1, 1, 7, 4)
        inter = intersection(r1, r2)
        self.assertEquals(Rectangle(6, 2, 2, 3), inter)

    def testR1InsideR2(self):
        r1 = Rectangle(6, 2, 4, 3)
        r2 = Rectangle(1, 1, 90, 40)
        inter = intersection(r1, r2)
        self.assertEquals(r1, inter)

class TestPolyRectSubtract(unittest.TestCase):
    def testNonIntersecting(self):
        r1 = Rectangle(0, 0, 2, 2)
        r2 = Rectangle(2, 2, 1, 1)
        p1 = PolyRect([r1, r2])

        r3 = Rectangle(5, 5, 3, 3)
        p2 = p1.subtract(r3)
        self.polyRectExact(p1, p2)

    def testInside(self):
        p1 = PolyRect([Rectangle(1, 1, 5, 4)])
        r = Rectangle(2, 2, 2, 2)

        p2 = p1.subtract(r)
        self.polyRectExact(PolyRect([
            Rectangle(1, 1, 5, 1),      # above intersection
            Rectangle(1, 4, 5, 1),      # below intersection
            Rectangle(1, 2, 1, 2),      # left of intersection
            Rectangle(4, 2, 2, 2),      # right of intersection
        ]), p2)

    def testDoubleSubtract(self):
        p1 = PolyRect([Rectangle(1, 1, 5, 4)])
        r = Rectangle(2, 2, 2, 2)

        p2 = p1.subtract(r)
        p3 = p2.subtract(r)
        self.polyRectExact(p2, p3)

    def testSubtractBottomRight(self):
        p1 = PolyRect([Rectangle(1, 1, 7, 4)])
        r = Rectangle(6, 2, 4, 4)

        p2 = p1.subtract(r)
        self.polyRectExact(PolyRect([
            Rectangle(1, 1, 7, 1),      # above intersection
            Rectangle(1, 2, 5, 3),      # left of intersection
        ]), p2)

    def testSubtractBottomRightThenInside(self):
        p1 = PolyRect([Rectangle(1, 1, 7, 4)])
        r = Rectangle(6, 2, 4, 4)

        p2 = p1.subtract(r)

        r2 = Rectangle(2, 2, 1, 2)
        p3 = p2.subtract(r2)
        self.polyRectExact(PolyRect([
            Rectangle(1, 1, 7, 1),      # above intersection r
            Rectangle(1, 4, 5, 1),      # below intersection r2
            Rectangle(1, 2, 1, 2),      # left of intersection r2
            Rectangle(3, 2, 3, 2),      # right of intersection r2
        ]), p3)

    def testSubtractBottomRightThenInsideThenCorner(self):
        p1 = PolyRect([Rectangle(1, 1, 7, 4)])
        r = Rectangle(6, 2, 4, 4)

        p2 = p1.subtract(r)

        r2 = Rectangle(2, 2, 1, 2)
        p3 = p2.subtract(r2)

        r3 = Rectangle(1, 3, 1, 3)
        p4 = p3.subtract(r3)

        self.polyRectExact(PolyRect([
            Rectangle(1, 1, 7, 1),      # above intersection r
            Rectangle(2, 4, 4, 1),      # below intersection r2 and right of r3
            Rectangle(1, 2, 1, 1),      # left of intersection r2 and above r3
            Rectangle(3, 2, 3, 2),      # right of intersection r2
        ]), p4)



    def polyRectExact(self, p1, p2):
        self.assertEquals(len(p1.rects), len(p2.rects))
        for i in range(0, len(p1.rects)):
            self.assertEquals(p1.rects[i], p2.rects[i],
                              "Element %d: %s != %s" % (
                                  i, repr(p1.rects[i]), repr(p2.rects[i])))

if __name__ == '__main__':
    unittest.main()
