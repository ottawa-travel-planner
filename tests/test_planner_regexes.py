#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

import unittest
import Planner


class TestErrorRegex(unittest.TestCase):

    def testBadFromAddress(self):
        text = """
            <html>blabbity blah
            
            <table cellpadding="0" cellspacing="0" summary="Warning message" class="warning" width="85%">
                <tr>
                    <td><img src="tripPlanning/images/imgWarning.gif"></td>
                    <td>The address you specified was not found.  Please enter another.</td>
                </tr>
            </table></html>"""
        match = Planner._error_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("msg"),
                          ("The address you specified was not found.  "
                           "Please enter another."))

    # I don't know if it ever spits out multi-line errors, but might as well
    # check it.
    def testMultiLineWarning(self):
        text = """
            <html>blabbity blah
            
            <table class="warning" width="85%">
                <tr>
                    <td>The address you specified was not found.""" "\n" \
                        """Please enter another.</td>
                </tr>
            </table></html>"""

        match = Planner._error_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("msg"),
                          ("The address you specified was not found.\n"
                           "Please enter another."))

    # This one has a lot of whitespace that we should cut.
    def testNoItinerariesError(self):
        text = """
            <table cellpadding="0" cellspacing="0" summary="Warning message" class="warning" width="85%">
	    <tr>
		<td><img src="tripPlanning/images/imgWarning.gif"></td>
		<td>
		
			
			
				No itineraries could be found.
			
				
		</td>
		</tr>
	    </table>"""
        match = Planner._error_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("msg"), "No itineraries could be found.")


if __name__ == '__main__':
    unittest.main()
