#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

import unittest
import time
import CommandParser
import PlanTime

class TestFullRx(unittest.TestCase):

    def testSimpleAddresses(self):
        text = "  101 champagne  to   18 auriga    "
        match = CommandParser._full_rx.match(text)
        self.assertEquals(match.group("start"), "101 champagne")
        self.assertEquals(match.group("end"), "18 auriga")

    def testSimpleAddressesWithTime(self):
        text = "101 champagne to  18 auriga   at 11:30 pm  "
        match = CommandParser._full_rx.match(text)
        self.assertEquals(match.group("start"), "101 champagne")
        self.assertEquals(match.group("end"), "18 auriga")
        self.assertEquals(match.group("rule"), "at")
        self.assertEquals(match.group("time"), "11:30 pm")

    def testIntersectionWithAtAsSecondLoc(self):
        text = "101 champagne to  auriga at antares "
        match = CommandParser._full_rx.match(text)
        self.assertEquals(match.group("start"), "101 champagne")
        self.assertEquals(match.group("end"), "auriga at antares")

    def testIntersectionWithAtAsSecondLocAndTime(self):
        text = "101 champagne to  auriga at antares  at   130  "
        match = CommandParser._full_rx.match(text)
        self.assertEquals(match.group("start"), "101 champagne")
        self.assertEquals(match.group("end"), "auriga at antares")
        self.assertEquals(match.group("rule"), "at")
        self.assertEquals(match.group("time"), "130")

class TestLocationRxes(unittest.TestCase):
    def testIntersectionRX(self):
        text = "antares at hunt club"
        match = CommandParser._intersection_rx.match(text)
        self.assertEquals(match.group(1), "antares")
        self.assertEquals(match.group(2), "hunt club")

    def testStopRX(self):
        text = "stop 6037"
        match = CommandParser._stop_rx.match(text)
        self.assertEquals(match.group(1), "6037")

    def testStopRXNoStop(self):
        text = "6037"
        match = CommandParser._stop_rx.match(text)
        self.assertEquals(match.group(1), "6037")

    def testStreetAddressRX(self):
        text = "1 kilbob way"
        self.assert_(CommandParser._address_rx.match(text))

class TestTimeDecoder(unittest.TestCase):
    def testMorningOneDigit(self):
        text = "stop 6037 to antares and hunt club at 9:30"
        cmd = CommandParser.CommandParser(text).cmd
        self.assertEquals(cmd.time.unixtime, today(9, 30))
        self.assertEquals(cmd.time.constraint, PlanTime.MUST_LEAVE_AFTER)

    def testMorningOneDigitNoColon(self):
        text = "stop 6037 to antares and hunt club at 930"
        cmd = CommandParser.CommandParser(text).cmd
        self.assertEquals(cmd.time.unixtime, today(9, 30))
        self.assertEquals(cmd.time.constraint, PlanTime.MUST_LEAVE_AFTER)

    def testMorningTwoDigits(self):
        text = "stop 6037 to antares and hunt club at 11:30"
        cmd = CommandParser.CommandParser(text).cmd
        self.assertEquals(cmd.time.unixtime, today(11, 30))
        self.assertEquals(cmd.time.constraint, PlanTime.MUST_LEAVE_AFTER)

    def testMorningTwoDigitsNoColon(self):
        text = "stop 6037 to antares and hunt club at 1130"
        cmd = CommandParser.CommandParser(text).cmd
        self.assertEquals(cmd.time.unixtime, today(11, 30))
        self.assertEquals(cmd.time.constraint, PlanTime.MUST_LEAVE_AFTER)

    def testPM(self):
        text = "stop 6037 to antares and hunt club at 8:30 pm"
        cmd = CommandParser.CommandParser(text).cmd
        self.assertEquals(cmd.time.unixtime, today(20, 30))
        self.assertEquals(cmd.time.constraint, PlanTime.MUST_LEAVE_AFTER)

    def testPMNoColon(self):
        text = "stop 6037 to antares and hunt club at 830 pm"
        cmd = CommandParser.CommandParser(text).cmd
        self.assertEquals(cmd.time.unixtime, today(20, 30))
        self.assertEquals(cmd.time.constraint, PlanTime.MUST_LEAVE_AFTER)

    def testPMNoMinute(self):
        text = "stop 6037 to antares and hunt club by 9pm"
        cmd = CommandParser.CommandParser(text).cmd
        self.assertEquals(cmd.time.unixtime, today(21, 0))
        self.assertEquals(cmd.time.constraint, PlanTime.MUST_ARRIVE_BEFORE)

    def testAMNoMinute(self):
        text = "stop 6037 to antares and hunt club by 9"
        cmd = CommandParser.CommandParser(text).cmd
        self.assertEquals(cmd.time.unixtime, today(9, 0))
        self.assertEquals(cmd.time.constraint, PlanTime.MUST_ARRIVE_BEFORE)

def today(hour, min):
    t = list(time.localtime())
    t[3] = hour
    t[4] = min
    return time.mktime(t)

if __name__ == '__main__':
    unittest.main()
