# vi: set fileencoding=latin1 softtabstop=4 shiftwidth=4 tabstop=8 expandtab:
#
# The above line tells Python to use Latin-1, which seems to be right for the
# accented characters coming back from the travel planner.

import unittest
import BusStopMashup

class TestStopRegex(unittest.TestCase):
    def testAccentedEInName(self):
        text = r"""<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3035' target='iframe'>3035</a></b></strong><small>  (RA970)</small><br>HERON STOP / ARR�T 4A <br><a href='iframe.asp?route=4&dir=10' target='iframe'>4</a> <a href='iframe.asp?route=87&dir=11' target='iframe'>87</a> <a href='iframe.asp?route=107&dir=11' target='iframe'>107</a> <a href='iframe.asp?route=111&dir=10' target='iframe'>111</a> <a href='iframe.asp?route=118&dir=10' target='iframe'>118</a> <a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> <a href='iframe.asp?route=656&dir=11' target='iframe'>656</a> </span>"""
        match = BusStopMashup._stop_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("stopnum"), "3035")
        self.assertEquals(match.group("code"), "RA970")

    def testLongStopCode(self):
        text = r"""<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3035' target='iframe'>3035</a></b></strong><small>  (SCO018)</small><br>HERON SCOTIABANK 2A<br></span>"""
        match = BusStopMashup._stop_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("code"), "SCO018")

    def testCodeTULIP(self):
        text = r"""span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=xxxx' target='iframe'>xxxx</a></b></strong><small>  (TULIP)</small><br>ARR�T TULIPES / TULIP STOP /  MACKENZIE/YORK<br></span>"""
        match = BusStopMashup._stop_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("code"), "TULIP")

    def testCodeTULIP13(self):
        text = r"""<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=xxxx' target='iframe'>xxxx</a></b></strong><small>  (TULIP13)</small><br>ARR�T TULIPES / TULIP STOP/ N.A.C. / C.N.A<br></span>"""
        match = BusStopMashup._stop_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("code"), "TULIP13")

    # Normally they set it to xxxx, but sometimes it's empty...
    def testEmpty560Num(self):
        text = r"""<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=' target='iframe'></a></b></strong><small>  (DT005)</small><br>O'CONNOR / SPARKS<br></span>"""
        match = BusStopMashup._stop_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("stopnum"), "")
        self.assertEquals(match.group("code"), "DT005")

    def testSnowIDWithSpaces(self):
        text = r"""<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=xxxx' target='iframe'>xxxx</a></b></strong><small>  (SNOW - 1)</small><br>ARR�T TULIPES / TULIP STOP/ N.A.C. / C.N.A<br><a href='iframe.asp?route=&dir=11' target='iframe'></a> </span>"""
        match = BusStopMashup._stop_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("code"), "SNOW - 1")

    def testSnowIDDashInWeirdPlaceMyTaxDollarsAtWork(self):
        text = r"""<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=xxxx' target='iframe'>xxxx</a></b></strong><small>  (SNOW- 26)</small><br>ARR�T TULIPES / TULIP STOP/ N.A.C. / C.N.A<br><a href='iframe.asp?route=&dir=10' target='iframe'></a> </span>"""
        match = BusStopMashup._stop_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("code"), "SNOW- 26")

    def testWinterludeStopShortCode(self):
        text = r"""<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=' target='iframe'></a></b></strong><small>  (STO)</small><br>WINTERLUDE STOP - ARR�T<br><a href='iframe.asp?route=&dir=10' target='iframe'></a> </span>"""
        match = BusStopMashup._stop_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("code"), "STO")
        self.assertEquals(match.group("name"), "WINTERLUDE STOP - ARR�T")
        self.assertEquals(match.group("stopnum"), "")

    def testTulipStopAccentedCode(self):
        text = r"""<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=xxxx' target='iframe'>xxxx</a></b></strong><small>  (SNO CAF�)</small><br>ARR�T TULIPES / TULIP PARC COMMISSIONERS PARK<br></span>"""
        match = BusStopMashup._stop_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("code"), "SNO CAF�")
        self.assertEquals(match.group("name"), "ARR�T TULIPES / TULIP PARC COMMISSIONERS PARK")
        self.assertEquals(match.group("stopnum"), "xxxx")

    def testNotreDameHighSchool(self):
        text = r"""<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=' target='iframe'></a></b></strong><small>  (NODE5443)</small><br>�.S. NOTRE DAME H.S.<br></span>"""
        match = BusStopMashup._stop_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("code"), "NODE5443")
        self.assertEquals(match.group("name"), "�.S. NOTRE DAME H.S.")
        self.assertEquals(match.group("stopnum"), "")




class TestStationRx(unittest.TestCase):
    def testBillingsBridge(self):
        text = r"""<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3034' target='iframe'>3034</a><br><a href='iframe.asp?route=bus_station&INFO_PHONE=3034&station_name=BILLINGS BRIDGE&station_id=BIB' target='iframe'>BILLINGS BRIDGE</b></strong></span>"""
        match = BusStopMashup._station_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("stopnum"), "3034")
        self.assertEquals(match.group("name"), "BILLINGS BRIDGE")
        self.assertEquals(match.group("code"), "BIB")

    def testSlaterBay(self):
        text = r"""<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3005' target='iframe'>3005</a><br><a href='iframe.asp?route=bus_station&INFO_PHONE=3005&station_name=SLATER / BAY&station_id=SLBA' target='iframe'>SLATER / BAY</b></strong></span>"""
        match = BusStopMashup._station_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("stopnum"), "3005")
        self.assertEquals(match.group("name"), "SLATER / BAY")
        self.assertEquals(match.group("code"), "SLBA")

    def testTunneysPasture(self):
        text = r"""<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3011' target='iframe'>3011</a><br><a href='iframe.asp?route=bus_station&INFO_PHONE=3011&station_name=TUNNEY'S PASTURE&station_id=TUP' target='iframe'>TUNNEY'S PASTURE</b></strong></span>"""
        match = BusStopMashup._station_rx.search(text)
        self.assertNotEquals(match, None)
        self.assertEquals(match.group("stopnum"), "3011")
        self.assertEquals(match.group("name"), "TUNNEY'S PASTURE")
        self.assertEquals(match.group("code"), "TUP")

 

if __name__ == '__main__':
    unittest.main()
