#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

"""Parses an HTML document into a DOMmy structure."""

from HTMLParser import HTMLParser, HTMLParseError

class HTMLTreeParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)

        self.root = Element(None)
        self.parseStack = [self.root]

    def getRoot(self):
        return self.root

    def handle_starttag(self, tag, attrs):
        newel = Element(tag, attrs)
        parent = self.parseStack[-1]
        parent.addChild(newel)
        if not tag in self.NO_CHILDREN:
            self.parseStack.append(newel)

    def handle_data(self, data):
        parent = self.parseStack[-1]

        # concatenate to previous text element if any
        if len(parent.elementList) > 0 and parent.elementList[-1].name is None:
            parent.elementList[-1].text += data
        else:
            textel = Element(None)
            textel.text = data
            parent.addChild(textel)

    def handle_endtag(self, tag):
        # We're not expecting closing tags for names in NO_CHILDREN, but
        # if they show up, whatever.
        if tag not in self.NO_CHILDREN:
            # Pop things off the stack until we find something that matches.
            for i in range(len(self.parseStack)):
                oldel = self.parseStack.pop()
                if oldel.name == tag:
                    break
            else:
                raise HTMLParseError("Unexpected close tag </" + tag + ">",
                                     self.getpos())

    # These tags don't have children (or </closing> tags.)
    NO_CHILDREN = set(("img", "hr", "p", "dd", "link", "meta"))

class Element:
    """Represents an HTML element or character data.

    Child elements are available through both a dictionary and a list.
    
    elementList is a list of immediate descendents.
    
    elementDict maps a tag name to a list of immediate descendents with
    that name.
    
    The current tag name is available as the member variable "name". For
    elements that are just data, name is None."""

    def __init__(self, elementName, attrs=[]):
        self.name = elementName
        self.elementList = []
        self.elementDict = dict()

        # dictify the attributes: duplicate attributes are dropped
        self.attrs = dict(attrs)

        self.text = None

    def addChild(self, newel):
        self.elementList.append(newel)

        if newel.name in self.elementDict:
            self.elementDict[newel.name].append(newel)
        else:
            self.elementDict[newel.name] = [newel]
