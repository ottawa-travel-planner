#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

import time

class PlanTime:
    """Defines a trip time and constraint (e.g. MUST_LEAVE_AFTER)."""
    def __init__(self, unixtime, constraint):
        self.unixtime = unixtime
        self.constraint = constraint

    def toPlannerParams(self):
        """Let's hope your timezone is Eastern. Otherwise this isn't going
           to return the right times. I don't see a way to locally override
           the timezone in Python without defining my own tzinfo subclass.
           I do not feel like installing pytz."""
        loc = time.localtime(self.unixtime)
        ymd = time.strftime("%Y-%m-%d", loc)
        params = (("dateSelect", ymd), ("day", ymd),
                  # visibleDay=June+6
                  ("visibleDay", time.strftime("%B ", loc) + str(loc.tm_mday)),
                  ("requestCode", self.constraint),
                  ("time", time.strftime("%I:%M", loc)),
                  ("timePeriod", time.strftime("%p", loc).lower()),
                  ("ok.x", 29), ("ok.y", 21))
        return params

APPROX_ARRIVAL_TIME = 0
MUST_ARRIVE_BEFORE = 1
APPROX_DEPARTURE_TIME = 2
MUST_LEAVE_AFTER = 3
FIRST_TRIP = 4
LAST_TRIP = 5
