


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Map</title>
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAIeACS_2sK2arfIj_30I44BQqS-dJDKaqPw-eQTOl9De2pBAu9BS2z6kfNDaqaMibDQcsyJc66aKRHw"
      type="text/javascript"></script>


<script type="text/javascript">
    //<![CDATA[

function load() {

    if (GBrowserIsCompatible()) {
      var map = new GMap(document.getElementById("map"));
map.addControl(new GSmallMapControl());
      map.centerAndZoom(new GPoint(-75.681008, 45.377663), 1);

 // Create our "tiny" marker icon
var icon = new GIcon();
icon.image = "http://octranspo.com/maps/busstop.gif";
icon.iconSize = new GSize(23, 23);
icon.iconAnchor = new GPoint(6, 20);
icon.infoWindowAnchor = new GPoint(5, 1);


// Creates one of our tiny markers at the given point
function createMarker(point, stuff) {
  var marker = new GMarker(point, icon);
  var html=stuff;
  GEvent.addListener(marker, "click", function() {
  marker.openInfoWindowHtml(html);
  });
return marker
}



var marker = createMarker(new GPoint(-75.6741767, 45.3863358), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4897' target='iframe'>4897</a></b></strong><small>  (RA040)</small><br>BANK / TRANSITWAY<br><a href='iframe.asp?route=1&dir=10' target='iframe'>1</a> <a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> <a href='iframe.asp?route=111&dir=10' target='iframe'>111</a> <a href='iframe.asp?route=141&dir=11' target='iframe'>141</a> <a href='iframe.asp?route=148&dir=10' target='iframe'>148</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6727121, 45.3851824), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=8295' target='iframe'>8295</a></b></strong><small>  (RA050)</small><br>BANK / OHIO<br><a href='iframe.asp?route=1&dir=11' target='iframe'>1</a> <a href='iframe.asp?route=5&dir=11' target='iframe'>5</a> <a href='iframe.asp?route=111&dir=11' target='iframe'>111</a> <a href='iframe.asp?route=141&dir=10' target='iframe'>141</a> <a href='iframe.asp?route=148&dir=11' target='iframe'>148</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6730067, 45.3851215), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=6464' target='iframe'>6464</a></b></strong><small>  (RA060)</small><br>BANK / OHIO<br><a href='iframe.asp?route=1&dir=10' target='iframe'>1</a> <a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> <a href='iframe.asp?route=111&dir=10' target='iframe'>111</a> <a href='iframe.asp?route=141&dir=11' target='iframe'>141</a> <a href='iframe.asp?route=148&dir=10' target='iframe'>148</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6719351, 45.3841779), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=8296' target='iframe'>8296</a></b></strong><small>  (RA070)</small><br>BANK / KILBORN<br><a href='iframe.asp?route=1&dir=11' target='iframe'>1</a> <a href='iframe.asp?route=5&dir=11' target='iframe'>5</a> <a href='iframe.asp?route=111&dir=11' target='iframe'>111</a> <a href='iframe.asp?route=141&dir=10' target='iframe'>141</a> <a href='iframe.asp?route=148&dir=11' target='iframe'>148</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6716528, 45.383393), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=8302' target='iframe'>8302</a></b></strong><small>  (RA080)</small><br>BANK / BELANGER<br><a href='iframe.asp?route=1&dir=10' target='iframe'>1</a> <a href='iframe.asp?route=111&dir=10' target='iframe'>111</a> <a href='iframe.asp?route=141&dir=11' target='iframe'>141</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6710713, 45.3829928), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=8469' target='iframe'>8469</a></b></strong><small>  (RA090)</small><br>BANK / ROCKINGHAM<br><a href='iframe.asp?route=1&dir=11' target='iframe'>1</a> <a href='iframe.asp?route=111&dir=11' target='iframe'>111</a> <a href='iframe.asp?route=141&dir=10' target='iframe'>141</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.672413, 45.3838034), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=8291' target='iframe'>8291</a></b></strong><small>  (RA210)</small><br>BELANGER / BANK<br><a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6741937, 45.3834114), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=8290' target='iframe'>8290</a></b></strong><small>  (RA220)</small><br>CLEMENTINE / BELANGER<br><a href='iframe.asp?route=5&dir=11' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6740882, 45.3836446), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4108' target='iframe'>4108</a></b></strong><small>  (RA230)</small><br>BELANGER / CLEMENTINE<br><a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.673093, 45.3835834), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4110' target='iframe'>4110</a></b></strong><small>  (RA240)</small><br>BELANGER / CLEMENTINE<br><a href='iframe.asp?route=5&dir=11' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6741672, 45.3826013), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4109' target='iframe'>4109</a></b></strong><small>  (RA250)</small><br>CLEMENTINE / ROCKINGHAM<br><a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6739146, 45.3824105), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=8289' target='iframe'>8289</a></b></strong><small>  (RA260)</small><br>CLEMENTINE / ROCKINGHAM<br><a href='iframe.asp?route=5&dir=11' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6740613, 45.3811068), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=8292' target='iframe'>8292</a></b></strong><small>  (RA270)</small><br>CLEMENTINE / GUERTIN<br><a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.673949, 45.380926), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4111' target='iframe'>4111</a></b></strong><small>  (RA280)</small><br>CLEMENTINE / GUERTIN<br><a href='iframe.asp?route=5&dir=11' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6737538, 45.3794218), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4114' target='iframe'>4114</a></b></strong><small>  (RA290)</small><br>CLEMENTINE / RICHARD<br><a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6736127, 45.3794658), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4112' target='iframe'>4112</a></b></strong><small>  (RA300)</small><br>CLEMENTINE / RICHARD<br><a href='iframe.asp?route=5&dir=11' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6733582, 45.3785281), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4115' target='iframe'>4115</a></b></strong><small>  (RA310)</small><br>CLEMENTINE / SECORD<br><a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6732797, 45.3786625), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4113' target='iframe'>4113</a></b></strong><small>  (RA320)</small><br>CLEMENTINE / HERON<br><a href='iframe.asp?route=5&dir=11' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6716024, 45.3781014), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=1095' target='iframe'>1095</a></b></strong><small>  (RA325)</small><br>HERON / KALADAR<br><a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6713382, 45.3769567), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=1096' target='iframe'>1096</a></b></strong><small>  (RA335)</small><br>KALADAR / GREGG<br><a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6715011, 45.3745462), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4116' target='iframe'>4116</a></b></strong><small>  (RA345)</small><br>BROOKFIELD / KALADAR<br><a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6735333, 45.374399), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4117' target='iframe'>4117</a></b></strong><small>  (RA355)</small><br>BROOKFIELD / CLEMENTINE<br><a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6758737, 45.3741279), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4118' target='iframe'>4118</a></b></strong><small>  (RA365)</small><br>BROOKFIELD / CLOVER<br><a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6762552, 45.3759934), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=6139' target='iframe'>6139</a></b></strong><small>  (RA375)</small><br>CLOVER / GREGG<br><a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6760637, 45.3777467), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=8233' target='iframe'>8233</a></b></strong><small>  (RA380)</small><br>HERON / CLOVER<br><a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> <a href='iframe.asp?route=5&dir=11' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6846606, 45.3774845), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=5602' target='iframe'>5602</a></b></strong><small>  (RA415)</small><br>HERON / BRONSON<br><a href='iframe.asp?route=87&dir=10' target='iframe'>87</a> <a href='iframe.asp?route=107&dir=10' target='iframe'>107</a> <a href='iframe.asp?route=111&dir=11' target='iframe'>111</a> <a href='iframe.asp?route=118&dir=11' target='iframe'>118</a> <a href='iframe.asp?route=140&dir=10' target='iframe'>140</a> <a href='iframe.asp?route=656&dir=10' target='iframe'>656</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6898542, 45.3768016), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=1569' target='iframe'>1569</a></b></strong><small>  (RA420)</small><br>HERON / RIVERSIDE<br><a href='iframe.asp?route=87&dir=11' target='iframe'>87</a> <a href='iframe.asp?route=107&dir=11' target='iframe'>107</a> <a href='iframe.asp?route=111&dir=10' target='iframe'>111</a> <a href='iframe.asp?route=118&dir=10' target='iframe'>118</a> <a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> <a href='iframe.asp?route=656&dir=11' target='iframe'>656</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.68547, 45.3771303), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=1342' target='iframe'>1342</a></b></strong><small>  (RA425)</small><br>HERON /  O TRAIN STN - STN O  BUS<br><a href='iframe.asp?route=87&dir=11' target='iframe'>87</a> <a href='iframe.asp?route=107&dir=11' target='iframe'>107</a> <a href='iframe.asp?route=111&dir=10' target='iframe'>111</a> <a href='iframe.asp?route=118&dir=10' target='iframe'>118</a> <a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> <a href='iframe.asp?route=656&dir=11' target='iframe'>656</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6901177, 45.3771274), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=1587' target='iframe'>1587</a></b></strong><small>  (RA430)</small><br>HERON / RIVERSIDE<br><a href='iframe.asp?route=87&dir=10' target='iframe'>87</a> <a href='iframe.asp?route=118&dir=11' target='iframe'>118</a> <a href='iframe.asp?route=656&dir=10' target='iframe'>656</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6855079, 45.3789393), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4091' target='iframe'>4091</a></b></strong><small>  (RA440)</small><br>BRONSON / HERON<br><a href='iframe.asp?route=4&dir=10' target='iframe'>4</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6855136, 45.3776525), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=2014' target='iframe'>2014</a></b></strong><small>  (RA444)</small><br>BRONSON / HERON<br></span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6850165, 45.377595), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=1004' target='iframe'>1004</a></b></strong><small>  (RA445)</small><br>BRONSON / HERON<br><a href='iframe.asp?route=4&dir=10' target='iframe'>4</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.685418, 45.3798655), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=5626' target='iframe'>5626</a></b></strong><small>  (RA450)</small><br>BRONSON / HERON<br><a href='iframe.asp?route=4&dir=11' target='iframe'>4</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6794933, 45.3833956), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=1559' target='iframe'>1559</a></b></strong><small>  (RA505)</small><br>BILLINGS BRIDGE / DATA CENTRE<br><a href='iframe.asp?route=107&dir=10' target='iframe'>107</a> <a href='iframe.asp?route=111&dir=11' target='iframe'>111</a> <a href='iframe.asp?route=140&dir=10' target='iframe'>140</a> <a href='iframe.asp?route=656&dir=10' target='iframe'>656</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6793181, 45.3831514), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=1567' target='iframe'>1567</a></b></strong><small>  (RA510)</small><br>DATA CENTRE / BILLINGS BRIDGE<br><a href='iframe.asp?route=4&dir=10' target='iframe'>4</a> <a href='iframe.asp?route=87&dir=11' target='iframe'>87</a> <a href='iframe.asp?route=107&dir=11' target='iframe'>107</a> <a href='iframe.asp?route=111&dir=10' target='iframe'>111</a> <a href='iframe.asp?route=118&dir=10' target='iframe'>118</a> <a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> <a href='iframe.asp?route=656&dir=11' target='iframe'>656</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6767042, 45.3846443), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3034' target='iframe'>3034</a></b></strong><small>  (RA900)</small><br>BILLINGS BRIDGE STOP/ARR�T 1A<br><a href='iframe.asp?route=4&dir=11' target='iframe'>4</a> <a href='iframe.asp?route=40&dir=11' target='iframe'>40</a> <a href='iframe.asp?route=43&dir=11' target='iframe'>43</a> <a href='iframe.asp?route=45&dir=11' target='iframe'>45</a> <a href='iframe.asp?route=82&dir=11' target='iframe'>82</a> <a href='iframe.asp?route=84&dir=10' target='iframe'>84</a> <a href='iframe.asp?route=87&dir=10' target='iframe'>87</a> <a href='iframe.asp?route=97&dir=10' target='iframe'>97</a> <a href='iframe.asp?route=107&dir=10' target='iframe'>107</a> <a href='iframe.asp?route=116&dir=11' target='iframe'>116</a> <a href='iframe.asp?route=118&dir=11' target='iframe'>118</a> <a href='iframe.asp?route=140&dir=10' target='iframe'>140</a> <a href='iframe.asp?route=146&dir=10' target='iframe'>146</a> <a href='iframe.asp?route=148&dir=10' target='iframe'>148</a> <a href='iframe.asp?route=604&dir=11' target='iframe'>604</a> <a href='iframe.asp?route=605&dir=11' target='iframe'>605</a> <a href='iframe.asp?route=656&dir=10' target='iframe'>656</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6769394, 45.3842771), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3034' target='iframe'>3034</a></b></strong><small>  (RA905)</small><br>BILLINGS BRIDGE STOP / ARR�T 2A<br><a href='iframe.asp?route=4&dir=10' target='iframe'>4</a> <a href='iframe.asp?route=40&dir=10' target='iframe'>40</a> <a href='iframe.asp?route=43&dir=10' target='iframe'>43</a> <a href='iframe.asp?route=45&dir=10' target='iframe'>45</a> <a href='iframe.asp?route=82&dir=10' target='iframe'>82</a> <a href='iframe.asp?route=84&dir=11' target='iframe'>84</a> <a href='iframe.asp?route=87&dir=11' target='iframe'>87</a> <a href='iframe.asp?route=97&dir=11' target='iframe'>97</a> <a href='iframe.asp?route=107&dir=11' target='iframe'>107</a> <a href='iframe.asp?route=116&dir=10' target='iframe'>116</a> <a href='iframe.asp?route=118&dir=10' target='iframe'>118</a> <a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> <a href='iframe.asp?route=146&dir=11' target='iframe'>146</a> <a href='iframe.asp?route=148&dir=11' target='iframe'>148</a> <a href='iframe.asp?route=605&dir=10' target='iframe'>605</a> <a href='iframe.asp?route=656&dir=11' target='iframe'>656</a> <a href='iframe.asp?route=891&dir=10' target='iframe'>891</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6751299, 45.3866486), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3034' target='iframe'>3034</a></b></strong><small>  (RA910)</small><br>BILLINGS BRIDGE / BANK<br><a href='iframe.asp?route=1&dir=10' target='iframe'>1</a> <a href='iframe.asp?route=1&dir=11' target='iframe'>1</a> <a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> <a href='iframe.asp?route=5&dir=11' target='iframe'>5</a> <a href='iframe.asp?route=111&dir=11' target='iframe'>111</a> <a href='iframe.asp?route=141&dir=10' target='iframe'>141</a> <a href='iframe.asp?route=148&dir=11' target='iframe'>148</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6767248, 45.3849864), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3034' target='iframe'>3034</a></b></strong><small>  (RA915)</small><br>BILLINGS BRIDGE STOP / ARR�T 3A<br><a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> <a href='iframe.asp?route=5&dir=11' target='iframe'>5</a> <a href='iframe.asp?route=141&dir=10' target='iframe'>141</a> <a href='iframe.asp?route=148&dir=11' target='iframe'>148</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6772016, 45.3846839), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3034' target='iframe'>3034</a></b></strong><small>  (RA920)</small><br>BILLINGS BRIDGE STOP/ARR�T 3B<br><a href='iframe.asp?route=8&dir=11' target='iframe'>8</a> <a href='iframe.asp?route=111&dir=11' target='iframe'>111</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6775756, 45.3844346), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3034' target='iframe'>3034</a></b></strong><small>  (RA925)</small><br>BILLINGS BRIDGE STOP / ARR�T 3C<br><a href='iframe.asp?route=140&dir=10' target='iframe'>140</a> <a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> <a href='iframe.asp?route=146&dir=10' target='iframe'>146</a> <a href='iframe.asp?route=604&dir=10' target='iframe'>604</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6779882, 45.3841586), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3034' target='iframe'>3034</a></b></strong><small>  (RA930)</small><br>BILLINGS BRIDGE STOP / ARR�T 3D<br></span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6776562, 45.3841562), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3034' target='iframe'>3034</a></b></strong><small>  (RA935)</small><br>BILLINGS BRIDGE STOP / ARR�T 4A<br></span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6772823, 45.3844055), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3034' target='iframe'>3034</a></b></strong><small>  (RA940)</small><br>BILLINGS BRIDGE STOP / ARR�T 4B<br><a href='iframe.asp?route=1&dir=10' target='iframe'>1</a> <a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> <a href='iframe.asp?route=111&dir=10' target='iframe'>111</a> <a href='iframe.asp?route=141&dir=11' target='iframe'>141</a> <a href='iframe.asp?route=148&dir=10' target='iframe'>148</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6769988, 45.3845834), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3034' target='iframe'>3034</a></b></strong><small>  (RA945)</small><br>BILLINGS BRIDGE STOP / ARR�T 4C<br><a href='iframe.asp?route=1&dir=11' target='iframe'>1</a> <a href='iframe.asp?route=5&dir=11' target='iframe'>5</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6766764, 45.3847971), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3034' target='iframe'>3034</a></b></strong><small>  (RA950)</small><br>BILLINGS BRIDGE STOP / ARR�T 4D<br><a href='iframe.asp?route=8&dir=10' target='iframe'>8</a> <a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> <a href='iframe.asp?route=146&dir=11' target='iframe'>146</a> <a href='iframe.asp?route=148&dir=11' target='iframe'>148</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6797892, 45.3788264), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3035' target='iframe'>3035</a></b></strong><small>  (RA955)</small><br>HERON STOP / ARR�T 1A<br><a href='iframe.asp?route=8&dir=11' target='iframe'>8</a> <a href='iframe.asp?route=40&dir=11' target='iframe'>40</a> <a href='iframe.asp?route=43&dir=11' target='iframe'>43</a> <a href='iframe.asp?route=45&dir=11' target='iframe'>45</a> <a href='iframe.asp?route=82&dir=11' target='iframe'>82</a> <a href='iframe.asp?route=84&dir=10' target='iframe'>84</a> <a href='iframe.asp?route=97&dir=10' target='iframe'>97</a> <a href='iframe.asp?route=116&dir=11' target='iframe'>116</a> <a href='iframe.asp?route=146&dir=10' target='iframe'>146</a> <a href='iframe.asp?route=605&dir=11' target='iframe'>605</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6795338, 45.3788336), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3035' target='iframe'>3035</a></b></strong><small>  (RA960)</small><br>HERON STOP / ARR�T 2A<br><a href='iframe.asp?route=8&dir=10' target='iframe'>8</a> <a href='iframe.asp?route=40&dir=10' target='iframe'>40</a> <a href='iframe.asp?route=43&dir=10' target='iframe'>43</a> <a href='iframe.asp?route=45&dir=10' target='iframe'>45</a> <a href='iframe.asp?route=82&dir=10' target='iframe'>82</a> <a href='iframe.asp?route=84&dir=11' target='iframe'>84</a> <a href='iframe.asp?route=97&dir=11' target='iframe'>97</a> <a href='iframe.asp?route=116&dir=10' target='iframe'>116</a> <a href='iframe.asp?route=146&dir=11' target='iframe'>146</a> <a href='iframe.asp?route=605&dir=10' target='iframe'>605</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6806668, 45.3790667), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3035' target='iframe'>3035</a></b></strong><small>  (RA965)</small><br>HERON STOP / ARR�T 3A<br><a href='iframe.asp?route=4&dir=11' target='iframe'>4</a> <a href='iframe.asp?route=87&dir=10' target='iframe'>87</a> <a href='iframe.asp?route=107&dir=10' target='iframe'>107</a> <a href='iframe.asp?route=111&dir=11' target='iframe'>111</a> <a href='iframe.asp?route=118&dir=11' target='iframe'>118</a> <a href='iframe.asp?route=140&dir=10' target='iframe'>140</a> <a href='iframe.asp?route=656&dir=10' target='iframe'>656</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6804633, 45.3790112), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3035' target='iframe'>3035</a></b></strong><small>  (RA970)</small><br>HERON STOP / ARR�T 4A <br><a href='iframe.asp?route=4&dir=10' target='iframe'>4</a> <a href='iframe.asp?route=87&dir=11' target='iframe'>87</a> <a href='iframe.asp?route=107&dir=11' target='iframe'>107</a> <a href='iframe.asp?route=111&dir=10' target='iframe'>111</a> <a href='iframe.asp?route=118&dir=10' target='iframe'>118</a> <a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> <a href='iframe.asp?route=656&dir=11' target='iframe'>656</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6848485, 45.3768469), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3063' target='iframe'>3063</a></b></strong><small>  (RA990)</small><br>O-TRAIN CONFEDERATION<br><a href='iframe.asp?route=&dir=10' target='iframe'></a> <a href='iframe.asp?route=&dir=11' target='iframe'></a> <a href='iframe.asp?route=750&dir=10' target='iframe'>750</a> <a href='iframe.asp?route=750&dir=11' target='iframe'>750</a> <a href='iframe.asp?route=&dir=10' target='iframe'></a> <a href='iframe.asp?route=&dir=11' target='iframe'></a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6907297, 45.3727134), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=8267' target='iframe'>8267</a></b></strong><small>  (RB040)</small><br>RIVERSIDE / BROOKFIELD<br><a href='iframe.asp?route=87&dir=11' target='iframe'>87</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6903514, 45.3696961), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4119' target='iframe'>4119</a></b></strong><small>  (RB050)</small><br>RIVERSIDE / AD. 2865<br><a href='iframe.asp?route=87&dir=10' target='iframe'>87</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.690323, 45.3707938), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4120' target='iframe'>4120</a></b></strong><small>  (RB060)</small><br>RIVERSIDE / AD. 2865<br><a href='iframe.asp?route=87&dir=11' target='iframe'>87</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6895694, 45.3681338), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4121' target='iframe'>4121</a></b></strong><small>  (RB070)</small><br>RIVERSIDE / RIDGEWOOD<br><a href='iframe.asp?route=87&dir=11' target='iframe'>87</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6859074, 45.3680088), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=2449' target='iframe'>2449</a></b></strong><small>  (RB310)</small><br>SPRINGLAND / RIDGEWOOD<br><a href='iframe.asp?route=140&dir=10' target='iframe'>140</a> <a href='iframe.asp?route=640&dir=11' target='iframe'>640</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6858616, 45.3685304), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=8903' target='iframe'>8903</a></b></strong><small>  (RB330)</small><br>SPRINGLAND / RIDGEWOOD<br><a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> <a href='iframe.asp?route=640&dir=10' target='iframe'>640</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6851432, 45.3696591), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4132' target='iframe'>4132</a></b></strong><small>  (RB340)</small><br>SPRINGLAND / HOBSON<br><a href='iframe.asp?route=140&dir=10' target='iframe'>140</a> <a href='iframe.asp?route=640&dir=11' target='iframe'>640</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6828794, 45.3699489), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4133' target='iframe'>4133</a></b></strong><small>  (RB350)</small><br>SPRINGLAND / NORBERRY<br><a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> <a href='iframe.asp?route=640&dir=10' target='iframe'>640</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6818513, 45.3704185), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=8914' target='iframe'>8914</a></b></strong><small>  (RB370)</small><br>FLANNERY / SPRINGLAND<br><a href='iframe.asp?route=140&dir=10' target='iframe'>140</a> <a href='iframe.asp?route=640&dir=11' target='iframe'>640</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6817343, 45.3705616), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=1583' target='iframe'>1583</a></b></strong><small>  (RB380)</small><br>FLANNERY / SPRINGLAND<br><a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> <a href='iframe.asp?route=640&dir=10' target='iframe'>640</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6821232, 45.3719232), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4136' target='iframe'>4136</a></b></strong><small>  (RB390)</small><br>FLANNERY / RAMSGATE<br><a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> <a href='iframe.asp?route=640&dir=10' target='iframe'>640</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6823767, 45.372051), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4134' target='iframe'>4134</a></b></strong><small>  (RB400)</small><br>FLANNERY / RAMSGATE<br><a href='iframe.asp?route=140&dir=10' target='iframe'>140</a> <a href='iframe.asp?route=640&dir=11' target='iframe'>640</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6834469, 45.3730935), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4135' target='iframe'>4135</a></b></strong><small>  (RB410)</small><br>FLANNERY / BROOKFIELD<br><a href='iframe.asp?route=140&dir=10' target='iframe'>140</a> <a href='iframe.asp?route=640&dir=11' target='iframe'>640</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6836214, 45.3733917), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4137' target='iframe'>4137</a></b></strong><small>  (RB420)</small><br>FLANNERY / BROOKFIELD<br><a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> <a href='iframe.asp?route=640&dir=10' target='iframe'>640</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6847316, 45.3734266), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=8913' target='iframe'>8913</a></b></strong><small>  (RB430)</small><br>BROOKFIELD / FLANNERY<br><a href='iframe.asp?route=117&dir=10' target='iframe'>117</a> <a href='iframe.asp?route=140&dir=10' target='iframe'>140</a> <a href='iframe.asp?route=640&dir=11' target='iframe'>640</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6832164, 45.3731458), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=0568' target='iframe'>0568</a></b></strong><small>  (RB439)</small><br>FLANNERY \ BROOKFIELD H.S.<br><a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6870935, 45.3734344), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4138' target='iframe'>4138</a></b></strong><small>  (RB440)</small><br>BROOKFIELD / HOBSON<br><a href='iframe.asp?route=117&dir=11' target='iframe'>117</a> <a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.685499, 45.3733331), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=0569' target='iframe'>0569</a></b></strong><small>  (RB449)</small><br>BROOKFIELD\ BROOKFIELD H.S.<br><a href='iframe.asp?route=640&dir=11' target='iframe'>640</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6868921, 45.373235), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4139' target='iframe'>4139</a></b></strong><small>  (RB450)</small><br>BROOKFIELD / HOBSON<br><a href='iframe.asp?route=117&dir=10' target='iframe'>117</a> <a href='iframe.asp?route=140&dir=10' target='iframe'>140</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6891924, 45.3730804), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=8263' target='iframe'>8263</a></b></strong><small>  (RB460)</small><br>BROOKFIELD / RIVERSIDE<br><a href='iframe.asp?route=117&dir=10' target='iframe'>117</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6902242, 45.3732407), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=6491' target='iframe'>6491</a></b></strong><small>  (RB470)</small><br>BROOKFIELD / RIVERSIDE<br><a href='iframe.asp?route=107&dir=10' target='iframe'>107</a> <a href='iframe.asp?route=111&dir=11' target='iframe'>111</a> <a href='iframe.asp?route=117&dir=11' target='iframe'>117</a> <a href='iframe.asp?route=140&dir=11' target='iframe'>140</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6885178, 45.3756132), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=0400' target='iframe'>0400</a></b></strong><small>  (RB481)</small><br>CANADA POST / HERON<br><a href='iframe.asp?route=107&dir=10' target='iframe'>107</a> <a href='iframe.asp?route=111&dir=11' target='iframe'>111</a> <a href='iframe.asp?route=140&dir=10' target='iframe'>140</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6886725, 45.3737246), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=0401' target='iframe'>0401</a></b></strong><small>  (RB491)</small><br>CANADA POST / BROOKFIELD<br><a href='iframe.asp?route=107&dir=10' target='iframe'>107</a> <a href='iframe.asp?route=111&dir=11' target='iframe'>111</a> <a href='iframe.asp?route=140&dir=10' target='iframe'>140</a> </span>");
map.addOverlay(marker);




var marker = createMarker(new GPoint(-75.6797761, 45.3788533), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3035' target='iframe'>3035</a></b></strong><small>  (SCO018)</small><br>HERON SCOTIABANK 2A<br></span>");
map.addOverlay(marker);




 // Create our "tiny" marker icon
var icon = new GIcon();
icon.image = "../transit_logos/Billings_Bridge.gif";
icon.iconSize = new GSize(126, 47);
icon.iconAnchor = new GPoint(6, 20);
icon.infoWindowAnchor = new GPoint(5, 1);



var marker = createMarker(new GPoint(-75.6769394, 45.3842771), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3034' target='iframe'>3034</a><br><a href='iframe.asp?route=bus_station&INFO_PHONE=3034&station_name=BILLINGS BRIDGE&station_id=BIB' target='iframe'>BILLINGS BRIDGE</b></strong></span>");
map.addOverlay(marker);
	


 // Create our "tiny" marker icon
var icon = new GIcon();
icon.image = "../transit_logos/Heron.gif";
icon.iconSize = new GSize(126, 47);
icon.iconAnchor = new GPoint(6, 20);
icon.infoWindowAnchor = new GPoint(5, 1);



var marker = createMarker(new GPoint(-75.6797892, 45.3788264), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3035' target='iframe'>3035</a><br><a href='iframe.asp?route=bus_station&INFO_PHONE=3035&station_name=HERON&station_id=HER' target='iframe'>HERON</b></strong></span>");
map.addOverlay(marker);
	



 // Create our "tiny" marker icon
var icon = new GIcon();
icon.image = "../transit_logos/Confederation_o.gif";
icon.iconSize = new GSize(122, 30);
icon.iconAnchor = new GPoint(6, 20);
icon.infoWindowAnchor = new GPoint(5, 1);



var marker = createMarker(new GPoint(-75.6849592, 45.376836), "<span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3063' target='iframe'>3063</a><br><a href='iframe.asp?route=bus_station&INFO_PHONE=3063&station_name=CONFEDERATION&station_id=CONF' target='iframe'>CONFEDERATION</b></strong></span>");
map.addOverlay(marker);
	



 // Create our "tiny" marker icon
var icon = new GIcon();
icon.image = "http://www.octranspo.com/maps/home.gif";
icon.iconSize = new GSize(33, 30);
icon.iconAnchor = new GPoint(6, 20);
icon.infoWindowAnchor = new GPoint(5, 1);

var marker = createMarker(new GPoint(-75.681008, 45.377663), "<span><strong>HERON RD & DATA CENTRE RD, OTTAWA, ON, CANADA</strong></span>");
map.addOverlay(marker);


}
}
    //]]>
    </script>
		<style type="text/css">
			html, body {
				width: 100%;
				height: 100%;
			}
			html {
				overflow: hidden
			}
			body {
				margin: 0px 0px 0px 0px;
				padding: 0px;
			}
			#map {
				width: 100%;
				height: 100%;
				border: 1px solid #979797;
				background-color: #e5e3df;
				margin: auto;

			}
		</style>

</head>

  <body onload="load()" onunload="GUnload()">
<br><div id="map"><div style="padding: 1em; color: gray">Loading...</div></div>


    
  </body>
</html>



