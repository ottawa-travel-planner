function validateAddr(theFormValue)
{
	temp = theFormValue.value;
	temp = trim(temp);
	if (temp == "")
	{
		alert("You must enter an address number and street name for the street address");
		return false;
	}
	else
	{
		
		separator = " ";
		tempArray = temp.split(separator);
		tempLength = tempArray.length;
		if (isNaN(tempArray[0]) || (tempLength < 2))
		{
			alert("You must enter an address number and street name for the street address");
			return false;
		}
	}
	return true;
}

function validate560(theFormValue)
{
	temp = theFormValue.value;
	if ((temp.length > 4) || (temp.length < 4))
	{
		alert("You must enter 4 digits for the 560-1000 number");		
		return false;
	}
	
	if (isNaN(temp) == true)
	{
		alert("You can enter only numbers from 0-9");
		return false;
	}
	return true;
}

function validateInte(theFormValue)
{
	temp = theFormValue.value;
	temp = trim(temp);
	if (temp == "")
	{
		alert("You must enter at least a letter for the on street");
		return false;
	}
	return true;
}

function trim(theString) 
{
	while (theString.substring(0,1) == " ")
		theString = theString.substring(1,theString.length);
	while (theString.substring(theString.length-1,theString.length) == " ")
		theString = theString.substring(0,theString.length-1);
	return theString;
}

function updateDate(theList,theTextBox,theVisibleDayField)
{
	optionValue = theList[theList.selectedIndex].value;
	if (optionValue != 'later')
	{
		theTextBox.value = optionValue;
		month = optionValue.substring(5,7);
		dayMonth = optionValue.substring(8,10);
		theVisibleDayField.value = setVisibleDay(month,dayMonth,"EN");
	}
	else
	{
		theTextBox.value = '';
		cal1.popup();
		theList.disabled = true;
	}
}

function getTodaysDate(travelDate,dropDown,theVisibleDayField)
{
	today = new Date();
	travelDate.value = getDateString(today,0);	
	dropDown.selectedIndex = 0;
	thisMonth = today.getMonth() + 1;
	theVisibleDayField.value = setVisibleDay(thisMonth.toString(),today.getDate().toString(),"EN");
}

function displayDates(numDays,language)
{
	today = new Date();

	for(i=0;i<numDays;i++)
	{
		if (i==0)
		{
			if (language == "FR")
				document.writeln("<option value=\"" + getDateString(today,0) + "\" selected>Aujourd'hui</option>");
			else
				document.writeln("<option value=\"" + getDateString(today,0) + "\" selected>Today</option>");
		}
		else
		{
			if (language == "FR")
				document.writeln("<option value=\"" + getDateString(today,1) + "\">" + getDayString(today,"FR") + "</option>");
			else
				document.writeln("<option value=\"" + getDateString(today,1) + "\">" + getDayString(today,"EN") + "</option>");
		}
	}
}

function getDateString(theDate,offset)
{
	dayInMS = 60 * 60 * 24 * 1000;
	if (offset > 0)
		theDate.setTime(theDate.getTime() + (offset * dayInMS));
	day = theDate.getDate();
	if (day < 10)
		day = "0" + day.toString();
	month = theDate.getMonth() + 1;
	if (month < 10)
		month = "0" + month.toString();		
	year = theDate.getFullYear();
	year = year.toString();
	return (year + '-' + month + '-' + day);
}

function getDayString(theDate,language)
{
	daysOfWeekEN = new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Invalid Day");
	daysOfWeekFR = new Array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Jour inadmissible");
	dayNumber = theDate.getDay();
	if (language == "FR")
		return daysOfWeekFR[dayNumber];
	else
		return daysOfWeekEN[dayNumber];
}

function validateTimeValue(theForm)
{
	if ((theForm.requestCode.selectedIndex != 4) && (theForm.requestCode.selectedIndex != 5))
	{
		tempTime = trim(theForm.time.value);
		if (tempTime.length <= 0)
		{
			alert("You must specify a time");
			return false;
		}
	}
	return true;
}

function setTripDate(theDate,theDay,theSelectControl,theVisibleDayField)
{
	month = theDate.substring(4,6);
	dayMonth = theDate.substring(6,8);
	theDate = theDate.substring(0,4) + "-" + month  + "-" + dayMonth;
	theDay.value = theDate;
	theSelectControl.disabled = true;
	theVisibleDayField.value = setVisibleDay(month,dayMonth,"EN");
}

function setVisibleDay(theMonth,theDay,language)
{
	monthEN = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
	monthFR = new Array("janvier", "f�vrier", "mars", "avril", "mai", "juin", "juillet", "ao�t", "septembre", "octobre", "novembre", "d�cembre");

	if (theMonth.substring(0,1) == 0)
		theMonth = parseInt(theMonth.substring(1,2));
	theMonth--;
	if (theDay.substring(0,1) == 0)
		theDay = parseInt(theDay.substring(1,2));	
		
	if (language == "FR")
	{
		if (theDay == 1)
			return "le 1er " + monthFR[theMonth];
		else
			return "le " + theDay.toString() + " " + monthFR[theMonth];
	}	
	else
		return monthEN[theMonth] + " " + theDay.toString();
}

function checkHistory(mode)
{
	if (mode == "from")
	{
		if (document.tp_fromHistory.historySelection) 
			document.tp_fromHistory.historySelection.focus();		
	}
	else if (mode == "to")
	{
		if (document.tp_toHistory.historySelection) 
			document.tp_toHistory.historySelection.focus();		
	}
}

function validateRouteNumber(theFormValue)
{
	temp = theFormValue.value;
	temp = trim(temp);
	if (temp.length == 0)
	{
		alert("You must enter a route number");		
		return false;
	}
	if (isNaN(temp) == true)
	{
		alert("You can enter only numbers from 0-9");
		return false;
	}
	return true;
}

function validateCheckboxes(thisForm,type)
{
	numChecked = 0;
	maxNumber = 5;
	if (type == 'route')
	{
		for (i=0;i<thisForm.length;i++)
		{
			if (thisForm.elements[i].type == "checkbox")
			{
				if (thisForm.elements[i].checked == true)
					numChecked++;
			}	
		}
	}
	else if (type == 'stop')
	{
		for (i=0;i<thisForm.length;i++)
		{
			if (thisForm.elements[i].type == "checkbox")
			{
				if (thisForm.elements[i].checked == true)
				{
					if(thisForm.elements[i].name != "accessible")
						numChecked++;
				}
			}	
		}
	}
	switch (numChecked)
	{
		case 0:
			if (type == 'route')
			{
				alert('You must select at least one route');
				return false;
			}	
			else if (type == 'stop')
			{
				alert('You must select at least one stop');
				return false;
			}
		break;
		
		default:
			if (numChecked > maxNumber)	
			{
				if (type == 'route')
				{
					alert('You must select between 1 to 5 routes');
					return false;
				}	
				else if (type == 'stop')
				{
					alert('You must select between 1 to 5 stops');
					return false;
				}
			}
		break;
	}
	return true;
}

function findScreenHeight()
{
	if (window.innerHeight)
		return window.innerHeight;
	else if (document.documentElement)
	{
		if (document.documentElement.clientHeight > 0)
			return document.documentElement.clientHeight;
		else
		{
			if	(document.body)
				return document.body.clientHeight;
		}
		return 0;
	}
}