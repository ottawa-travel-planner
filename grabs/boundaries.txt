west: just east of west ridge & hazeldean - 45.264678,-75.941191

east: old montreal & kinsella - 45.519278,-75.378571

north: halton terrace - 45.352025,-75.939775

south: bankfield rd - 45.224854,-75.671597

north-south delta: 0.127171 (14.16 km)
east-west delta: 0.562620 (about 44 km)

Official stop mashup returns stops within about 0.01 degree radius from home
location (so about 1 km radius north-south, 0.75 km east-west)
