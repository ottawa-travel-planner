#!/usr/bin/python
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

import email.FeedParser
import email.iterators
from email.MIMEText import MIMEText
from email.Utils import parseaddr
import smtplib
import sys
import traceback
import syslog
import re

import Planner
import PlanTime
import time
import CommandParser
import ShortFormatter

FROM_ADDRESS = "oc@hurts.ca"
SMTP_SERVER = "127.0.0.1"

HELP_TEXT = """Examples:
31 antares to 81 florence
stop 6097 to bayshore at 830 pm
merivale and baseline to 6097 by 9"""

def logIt(string):
    syslog.syslog(syslog.LOG_INFO, string)

def sendMessage(msg, to):
    msg['From'] = FROM_ADDRESS
    msg['To'] = to

    s = smtplib.SMTP(SMTP_SERVER)
    s.sendmail(parseaddr(FROM_ADDRESS)[1], [parseaddr(msg['To'])[1]],
                         msg.as_string())
    s.close()

def sendError(e, replyto):
    sendMessage(MIMEText(e.args[0]), replyto)

def sendHelp(replyto):
    sendMessage(MIMEText(HELP_TEXT), replyto)

def findFirstLine(msg):
    for line in msg.get_payload(None, True).splitlines():
        s = line.strip()
        if len(s) != 0:
            return s
    return None

def handleMessage(msg, replyto):
    try:
        s = None

        # Look for a text/plain part.
        for subpart in email.iterators.typed_subpart_iterator(msg, "text",
                                                              "plain"):
            s = findFirstLine(subpart)
            if s is not None:
                break
        else:
            # No text/plain sub-parts, or the message body was empty
            raise Exception("Your message was empty, or was not in plain "
                            + "text format.\n\n"
                            + "For help, send the word 'help' by itself.\n")

        if s is None:
            logIt("Empty command from %s" % replyto)
            sendHelp(replyto)
	    return 0

        logIt(s)
        if s.lower().startswith("help"):
            sendHelp(replyto)
            return 0

        cmd = CommandParser.CommandParser(s).cmd

        # Leave in 3 minutes if time is unspecified.
        if cmd.time is None:
            cmd.time = PlanTime.PlanTime(time.time() + 180,
                                         PlanTime.MUST_LEAVE_AFTER)

        itin = Planner.plan(cmd.start, cmd.end, cmd.time)
        sf = ShortFormatter.ShortFormatter(itin.entries)

        text = "\n".join(sf.lines)
        if itin.anyUnparsed():
            text += "\nWarning: This itinerary display may be incomplete!"

        logIt("success, %d entries" % len(itin.entries))
        sendMessage(MIMEText(text), replyto)
    except Exception, e:
        logIt("Exception: %s" % e)
        logIt(traceback.format_exc())
        sendError(e, replyto)

    # Always return success. Otherwise Postfix will send a bounce message.
    return 0

def looksLikePostmaster(msg, replyto):
    r = replyto.lower()

    # from the vacation(1) manpage plus some extras
    for s in ('-request@', 'postmaster', 'uucp', 'mailer-daemon', 'mailer',
            '-relay', 'abuse@', 'mailerdaemon', 'majordomo', 'maildaemon'):
        if r.find(s) != -1:
            return True

    p = msg["precedence"]
    if p and p.lower() in ('bulk', 'junk', 'list'):
        return True

    return False

def main():
    fp = email.FeedParser.FeedParser()
    for line in sys.stdin:
        fp.feed(line)
    msg = fp.close()

    # if these headers are here, we can reply semi-intelligently with errors
    replyto = msg["reply-to"] or msg["from"]
    if looksLikePostmaster(msg, replyto):
        logIt("Skipping reply to postmaster address '%s'" % replyto)
        return 0

    return handleMessage(msg, replyto)

if __name__ == '__main__':
    syslog.openlog(sys.argv[0], syslog.LOG_DAEMON)
    sys.exit(main())
