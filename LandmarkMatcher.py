#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

"""Matches partial landmark names to full landmark names using a
   prioritized list.

   It turns out that you can send a partial name to the travel planner
   web app and it'll figure out what you mean, but there's no
   prioritization of partial matches: BAYSHORE maps to
   BAYSHORE CATHOLIC SCHOOL before it maps to BAYSHORE SHOPPING CENTRE.

   We do our own prioritization by using landmarkLeech.py to grab
   an offline copy of the landmark list in landmarks.txt, and
   listing the categories (transit station, shopping centre, etc.) in
   priority order in landmarkCategoryPriority.txt."""

from PlannerExceptions import *

class LandmarkMatcher:
    def __init__(self, listfile="landmarks.txt",
                 priofile="landmarkCategoryPriority.txt"):

        self.all = set()

        # List of categories; each one contains a list of landmarks
        self.categories = []

        # Map of category to landmark list.
        self.categoryMap = {}

        self._loadLandmarks(listfile)
        self._loadCategoryPrio(priofile)

    def _loadLandmarks(self, listfile):
        listfp = open(listfile, "r")
        for line in listfp:
            parts = line.rstrip().split("|")
            landmark = parts[1].upper()
            self.all.add(landmark)

            if not parts[0] in self.categoryMap:
                self.categoryMap[parts[0]] = []
            self.categoryMap[parts[0]].append(landmark)

    def _loadCategoryPrio(self, priofile):
        priofp = open(priofile, "r")
        for line in priofp:
            self.categories.append(self.categoryMap[line.rstrip()])

    def match(self, str):
        str = str.upper()
        if str in self.all:
            return str

        for catlist in self.categories:
            for landmark in catlist:
                if landmark.startswith(str):
                    return landmark

        raise InvalidLandmarkException("Couldn't find landmark '%s' " 
                                       "in local database." % str)
