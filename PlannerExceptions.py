#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

"""usage: from PlannerExceptions import *"""

class TravelPlannerException(Exception):
    """Generic parent exception for anything thrown by us, as opposed to
       things thrown by the network layer."""
       
    def __init__(self, value):
        Exception.__init__(self, value)

class InvalidLandmarkException(TravelPlannerException):
    """Thrown when a source or destination landmark can't be found."""

    def __init__(self, value):
        TravelPlannerException.__init__(self, value)

class ItineraryParseException(TravelPlannerException):
    """Thrown when the itinerary returned by the travel planner can't be
       parsed."""

    def __init__(self, msg):
        TravelPlannerException.__init__(self, msg)
