#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

"""Downloads Google Maps bus stop locations using the official mashup."""

import urllib
import urllib2
import sys
import re

class StopRoute(object):
    def __init__(self, number, direction):
        self.number = number        # route number
        self.direction = direction  # route direction, seems to be 10 or 11

    def __repr__(self):
        return "route(%s,%s)" % (self.number, self.direction)

class StopLocation(object):
    def __init__(self, latitude, longitude):
        self.latitude = float(latitude)
        self.longitude = float(longitude)

    def __repr__(self):
        return "loc (%s,%s)" % (self.latitude, self.longitude)

class BaseStop(object):
    def __init__(self):
        self.number = None          # 560-1000 number
                                    # This is not necessarily unique - there
                                    # are multiple stops #3034, for each
                                    # platform at Billings Bridge
        self.code = None            # e.g. WD360 - looks to be unique
        self.name = None            # usually an intersection
        self.location = None

        self.requestedAddress = None  # search address that returned this stop

    def basestr(self):
        return "560 %s,%s,%s,%s" % (
                self.number, repr(self.code), repr(self.name), self.location)

    def __repr__(self):
        return "basestop (%s)" % self.basestr()

class Stop(BaseStop):
    """A regular bus stop."""
    def __init__(self):
        super(Stop, self).__init__()
        self.routes = []            # list of StopRoutes at this stop

    def __repr__(self):
        return "stop (%s): %s" % (
                    self.basestr(), ", ".join([str(r) for r in self.routes]))

class Station(BaseStop):
    """A Transitway or O-Train station."""
    def __init__(self):
        super(Station, self).__init__()
        # code will be short (3- or 4-letter) station identifier
        # name will be station name

    def __repr__(self):
        return "station (%s)" % self.basestr()

class UnknownStopType(BaseStop):
    def __init__(self, location, html):
        super(UnknownStopType, self).__init__()
        self.location = location
        self.html = html

    def __repr__(self):
        return "UNKNOWN (%s, %s)" % (self.location, repr(self.html))

class HomeLocation(BaseStop):
    def __init__(self, location, requestedAddress, respondedAddress):
        super(HomeLocation, self).__init__()
        self.location = location
        self.requestedAddress = requestedAddress
        self.respondedAddress = respondedAddress

    def __repr__(self):
        return "HOME: (asked %s, got %s, %s)" % (
                repr(self.requestedAddress), repr(self.respondedAddress),
                self.location)

class Marker(object):
    def __init__(self, location, html):
        self.location = location
        self.html = html

    def __repr__(self):
        return "marker (%s, %s)" % (self.location, repr(self.html))

class InvalidAddressException(Exception):
    def __init__(self, address):
        super(InvalidAddressException, self).__init__(address)

class Client(object):
    def findStops(self, address):
        html = self._grabHTML(address)
        self._checkForErrors(html, address)
        for marker in self._findMarkers(html):
            for (rx, func) in (
                    (_station_rx, self._parseStation),
                    (_stop_rx, self._parseStop),
                    (_home_rx, self._parseHome)):

                match = rx.search(marker.html)
                if match is not None:
                    ret = func(address, marker, match)
                    if ret is not None:
                        yield ret
                    break
            if match is None:
                yield UnknownStopType(marker.location, marker.html)

    def _grabHTML(self, address):
        params = { 'address': address }
        params.update(FIXED_PARAMS)
        f = urllib2.urlopen(URL, urllib.urlencode(params))

        html = ""
        for line in f:
            html += line
        f.close()

        return html

    def _checkForErrors(self, html, address):
        if _error_rx.search(html) is not None:
            raise InvalidAddressException(address)

    def _findMarkers(self, html):
        for m in _marker_rx.finditer(html):
            yield Marker(StopLocation(m.group("latitude"),
                                      m.group("longitude")),
                         m.group("html").decode(REAL_ENCODING))

    def _fillBaseStop(self, address, marker, match, stop):
        stop.number = match.group("stopnum")
        stop.code = match.group("code")
        stop.name = match.group("name").strip()
        stop.location = marker.location
        stop.requestedAddress = address

    def _parseStation(self, address, marker, match):
        stop = Station()
        self._fillBaseStop(address, marker, match, stop)
        return stop

    def _parseStop(self, address, marker, match):
        stop = Stop()
        self._fillBaseStop(address, marker, match, stop)

        for m in _stop_route_rx.finditer(marker.html):
            stop.routes.append(
                    StopRoute(m.group("routenum"), m.group("direction")))
        return stop

    def _parseHome(self, address, marker, match):
        return HomeLocation(marker.location, address, match.group("homeaddr"))

URL = "http://www.octranspo.com/maps/busstops/imap.asp"
FIXED_PARAMS = { 'page': 'search' }

# There's a meta tag that says it's UTF-8, but it turns out it's latin-1.
REAL_ENCODING = "latin-1"

# Any type of marker
# var marker = createMarker(new GPoint(..., ...), "blah blah");
# note: gmaps api v1 uses GPoint(long, lat) instead of GLatLng(lat, long)
_marker_re = (r'createMarker\(new GPoint\('
              r'(?P<longitude>[^,]+),\s*(?P<latitude>[^,]+)\),\s*'
              # ?s: DOTALL: . matches \n
              # *? is non-greedy
              r'"(?s)(?P<html>.*?)"\);\r?\n')
_marker_rx = re.compile(_marker_re)


# Marker for a transitway station
# <span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=3034' target='iframe'>3034</a><br><a href='iframe.asp?route=bus_station&INFO_PHONE=3034&station_name=BILLINGS BRIDGE&station_id=BIB' target='iframe'>BILLINGS BRIDGE</b></strong></span>
# (?i): Make it case-insensitive
# For station name, assume they'll URL-encode it properly someday
_station_re = (r'(?i)INFO_PHONE=(?P<stopnum>\w+)'
               r'.*station_name=(?P<name>[^<>&]+)'
               r'.*station_id=(?P<code>\w+)')
_station_rx = re.compile(_station_re)

# Marker for a regular stop
# <span><strong><b>613-560-1000 plus <a href='iframe.asp?route=busstop&INFO_PHONE=4897' target='iframe'>4897</a></b></strong><small>  (RA040)</small><br>BANK / TRANSITWAY<br><a href='iframe.asp?route=1&dir=10' target='iframe'>1</a> <a href='iframe.asp?route=5&dir=10' target='iframe'>5</a> <a href='iframe.asp?route=111&dir=10' target='iframe'>111</a> <a href='iframe.asp?route=141&dir=11' target='iframe'>141</a> <a href='iframe.asp?route=148&dir=10' target='iframe'>148</a> </span>
_stop_re = (r'(?i)INFO_PHONE=(?P<stopnum>\w*)'
            r'.*?\<small\>\s*\((?P<code>[^)]+)\)'
            r'.*\<br\>(?P<name>[^<>]+)\<br\>')
_stop_rx = re.compile(_stop_re)

_stop_route_re = (r'(?i)route=(?P<routenum>\w+)&dir=(?P<direction>\w+)')
_stop_route_rx = re.compile(_stop_route_re)

# Marker for the current location
# <span><strong>HERON RD & DATA CENTRE RD, OTTAWA, ON, CANADA</strong></span>"
_home_re = (r'(?i)(?P<homeaddr>[^<>]+), CANADA')
_home_rx = re.compile(_home_re)

_error_re = (r'G_GEO_UNKNOWN_ADDRESS')
_error_rx = re.compile(_error_re)

def main(argv=None):
    if argv is None:
        argv = sys.argv

    cmdstr = " ".join(argv[1:])
    for s in Client().findStops(cmdstr):
        print s

if __name__ == '__main__':
    sys.exit(main())
