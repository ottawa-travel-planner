#!/bin/sh

# Read each line from seeds.txt into an argument. We have to set IFS to '\n'.
oldIFS="$IFS"
IFS="
"
set -- $(cat ../grabs/seeds.txt)
IFS="$oldIFS"

# Use Privoxy + Tor
http_proxy=127.0.0.1:8118 ./stopMashupLeech.py "$@"
