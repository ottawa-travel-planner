#!/usr/bin/python
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

import sys
import time
import random
import re
import pickle
import signal
import getopt
import tempfile
import os
from xml.etree import ElementTree

import sys
sys.path.append("..")

import LatLongTools
import RectTools
import BusStopMashup

class SearchedAddress(object):
    def __init__(self, referrer, address):
        self.referrer = referrer
        self.address = address
        self.location = None

    def __repr__(self):
        return "searchaddr (referrer %s, addr %s, location %s)" % (
                self.referrer, self.address, self.location)

class Leech(object):
    def __init__(self, sleepRange):
        self.sleepRange = sleepRange

        # All stops we've found so far, indexed by code.
        self.allStops = {}

        # Stops whose addresses we've fed back into the web application.
        self.searched = []

        # Stops we weren't able to parse properly.
        # Each entry is a tuple: (UnknownStopType, SearchedAddress)
        self.unparsedStops = []

        # Stops whose names we weren't able to convert to addresses.
        self.unparsedStopNames = []

        self.client = BusStopMashup.Client()

        self.progressListener = lambda searchaddr, leech: True
        self.firstTime = True
        self.bail = False

    def bailEarly(self):
        self.bail = True

    def followSeed(self, seed):
        print >> sys.stderr, "SEED %s" % seed

        oall = len(self.allStops)
        osearch = len(self.searched)

        sa = SearchedAddress(seed, seed)
        self.recursiveSearch(sa)
        print >> sys.stderr, \
                "END SEED %s: searched %d addresses, found %d new stops" % (
                seed, len(self.searched) - osearch, len(self.allStops) - oall)

    def recursiveSearch(self, searchaddr):
        candidates = self.searchAddress(searchaddr)

        # By now sa.location should be filled in with the center location.
        # Drop out if the address was invalid.
        if searchaddr.location is None:
            return False

        corners = LatLongTools.findCorners(searchaddr.location,
                            [(stop.location, stop) for stop in candidates])

        for c in corners:
            for stop in c:
                if self.bail:
                    return True

                addr = self.stopToAddress(stop)
                if addr is None:
                    continue

                if addr in [sa.address for sa in self.searched]:
                    # Already been here. Skip this corner.
                    break

                if radiusCompletelyCoveredByOtherPoints(stop.location,
                        [s.location for s in self.searched
                            if s is not searchaddr and s.location is not None],
                        0.019, 0.019):
                    # Already been close to here. Skip this corner.
                    break

                newsa = SearchedAddress(stop, addr)
                if self.recursiveSearch(newsa):
                    # Valid address. Stop looking in this corner.
                    break

        return True

    def stopToAddress(self, stop):
        # The mashup doesn't like accented characters, so try a simple
        # replacement.
        name = anglicize(stop.name)

        if not isinstance(stop, BusStopMashup.Stop):
            # Only regular bus stops tend to have names that we can use as
            # addresses.
            return None
        if _station_stop_rx.match(name) is not None:
            # Ignore Transitway station stops.
            return None
        if _tulip_rx.search(name) is not None:
            # Ignore Tulipfest stops (ARRET TULIPES / TULIP STOP / ...)
            return None

        # RIVERSIDE / AD. 2865 -> 2865 RIVERSIDE
        m = _addr_rx.match(name)
        if m is not None:
            return "%s %s" % (m.group("number"), m.group("street1"))

        # BANK / RIVERSIDE -> BANK & RIVERSIDE
        m = _intersection_rx.match(name)
        if m is not None:
            return "%s & %s" % (m.group("street1"), m.group("street2"))

        self.unparsedStopNames.append(stop)

    def searchAddress(self, searchaddr):
        self.pause()

        self.searched.append(searchaddr)
        try:
            stops = list(self.client.findStops(searchaddr.address))
        except BusStopMashup.InvalidAddressException, e:
            print >> sys.stderr, "\tINVALID ADDR %s (referrer %s)" % (
                    searchaddr.address, searchaddr.referrer)
            return []
        except IOError, e:
            print >> sys.stderr, "\tFAILED SEARCH %s: %s (referrer %s)" % (
                    searchaddr.address, e, searchaddr.referrer)
            return []

        oldstops = 0
        newstops = 0
        followCandidates = []
        for stop in stops:
            if isinstance(stop, BusStopMashup.HomeLocation):
                # Record the actual center location, which is probably
                # slightly off from the location of the stop we fed in
                # since we're looking at the intersection and not the stop.
                searchaddr.location = stop.location
            elif isinstance(stop, BusStopMashup.UnknownStopType):
                self.unparsedStops.append((stop, searchaddr))
            else:
                # Even if we've seen a stop before, we should consider it as
                # a possible search location.
                followCandidates.append(stop)
                if self.allStops.has_key(stop.code):
                    oldstops += 1
                else:
                    newstops += 1
                    self.allStops[stop.code] = stop

        print >> sys.stderr, "\t%s (referrer %s): %d stops, %d new" % (
            searchaddr.address, searchaddr.referrer,
            oldstops + newstops, newstops)

        self.progressListener(searchaddr, self)

        return followCandidates

    def pause(self):
        if self.firstTime:
            self.firstTime = False
        else:
            time.sleep(random.uniform(*self.sleepRange))

# Matches a location like 'RIVERSIDE / AD. 2865'
_addr_re = (r'^(?P<street1>[^/]+?)\s*/\s*AD\.\s*(?P<number>\d+)$')
_addr_rx = re.compile(_addr_re)

# Matches a regular intersection, 'STREET1 / STREET2'
_intersection_re = (r'^(?P<street1>[^/]+?)\s*/\s*(?P<street2>[^/]+?)\s*$')
_intersection_rx = re.compile(_intersection_re)

# Matches a transitway stop. We'll have to ignore these.
_station_stop_re = (r'.*STOP\s*/ ARR.*')
_station_stop_rx = re.compile(_station_stop_re)

# Tulipfest. Formatting is not consistent; ignore
_tulip_re = (r'TULIPES')
_tulip_rx = re.compile(_tulip_re)

def anglicize(ucodestr):
    return ucodestr.translate({
        0xc0: u'A',  0xc1: u'A', 0xc2: u'A', 0xc3: u'A', 0xc4: u'A', 0xc5: u'A',
        0xc6: u'AE', 0xc7: u'C',
        0xc8: u'E',  0xc9: u'E', 0xca: u'E', 0xcb: u'E',
        0xcc: u'I',  0xcd: u'I', 0xce: u'I', 0xcf: u'I',
        0xd2: u'O',  0xd3: u'O', 0xd4: u'O', 0xd5: u'O', 0xd6: u'O', 0xd8: u'O',
        0xd9: u'U',  0xda: u'U', 0xdb: u'U', 0xdc: u'U',
        0xdd: u'Y',
        0xe0: u'a',  0xe1: u'a', 0xe2: u'a', 0xe3: u'a', 0xe4: u'a', 0xe5: u'a',
        0xe6: u'ae',
        0xe8: u'e',  0xe9: u'e', 0xea: u'e', 0xeb: u'e',
        0xec: u'i',  0xed: u'i', 0xee: u'i', 0xef: u'i',
        0xf2: u'o',  0xf3: u'o', 0xf4: u'o', 0xf5: u'o', 0xf6: u'o', 0xf8: u'o',
        0xf9: u'u',  0xfa: u'u', 0xfb: u'u', 0xfc: u'u', 0xfd: u'u',
        0xfd: u'y'}).encode("ascii")


def radiusCompletelyCoveredByOtherPoints(location, locations, width, height):
    p = RectTools.PolyRect([RectTools.Rectangle(location.longitude - width/2,
                                                location.latitude - height/2,
                                                width, height)])

    # Require at least 2% new coverage.
    min_area = 0.02 * width * height
    for i in locations:
        p = p.subtract(RectTools.Rectangle(i.longitude - width/2,
                                           i.latitude - width/2,
                                           width, height))

        if p.area() < min_area:
            break

    return p.area() < min_area

def usage():
    print >> sys.stderr, \
"""Usage: ./stopMashupLeech.py [--fast] seed1 [seed2 [...]]

Don't use --fast except for brief test runs."""
    sys.exit(1)

def doBail(leech):
    print >> sys.stderr, "Exiting after current search completes."
    leech.bailEarly()

def progressListener(searchaddr, leech):
    saveAll(leech)

def saveAll(leech):
    dumpXML(STOPS_XML, leech.allStops)
    dumpSearchedXML(SEARCHADDR_XML, leech.searched)

def tryResume(leech):
    try:
        stopf = open(STOPS_XML, "r")
        leech.allStops = unxmlifyStops(stopf)
        stopf.close()
        print >> sys.stderr, "Loaded %d previous stops." % len(leech.allStops)

        searchf = open(SEARCHADDR_XML, "r")
        leech.searched = unxmlifySearched(searchf)
        searchf.close()
        print >> sys.stderr, "Loaded %d previously searched addresses." % (
                    len(leech.searched))
    except IOError, e:
        pass
    # XML parser exceptions will cause a program exit

def main(argv):
    sleepRange = (38, 66)

    try:
        opts, seeds = getopt.getopt(argv[1:], "fh", ["fast", "help"])
        for o, a in opts:
            if o in ('-f', '--fast'):
                sleepRange = (3, 10)
            elif o in ('-h', '--help'):
                usage()
            else:
                usage()
    except getopt.GetoptError, e:
        print e
        usage()

    if len(seeds) == 0:
        usage()

    leech = Leech(sleepRange)
    leech.progressListener = progressListener

    tryResume(leech)

    sighandler = lambda sig, frame: doBail(leech)
    signal.signal(signal.SIGINT, sighandler)
    signal.signal(signal.SIGTERM, sighandler)

    for seed in seeds:
        if not leech.bail:
            leech.followSeed(seed)

    print "Locations searched: %d" % len(leech.searched)
    print "Stops loaded: %d" % len(leech.allStops)

    print "\nUnparsed stops: %d" % len(leech.unparsedStops)
    for s in leech.unparsedStops:
        print "\t%s; returned in search for %s" % (s[0], s[1].address)

    print "\nUnparsed stop names: %d" % len(leech.unparsedStopNames)
    for s in leech.unparsedStopNames:
        print "\t%s" % repr(s)

    inval = [sa for sa in leech.searched if sa.location is None]
    print "\nInvalid locations searched: %d" % len(inval)
    for i in inval:
        print "\t%s" % repr(i)

    saveAll(leech)

def safeWrite(filename, data, dumpfunc):
    (fd, n) = tempfile.mkstemp(dir='.', prefix=filename)
    f = os.fdopen(fd, "w")
    dumpfunc(data, f)
    f.close()

    try:
        os.unlink(filename)
    except:
        pass
    os.link(n, filename)
    os.unlink(n)

def dumpXML(filename, data):
    safeWrite(filename, data, xmlifyStops)

def dumpSearchedXML(filename, data):
    safeWrite(filename, data, xmlifySearched)

def xmlifyStops(data, f):
    root = ElementTree.Element("stops")
    root.text = '\n'
    root.tail = '\n'
    for (code, stop) in data.iteritems():
        attribs = {}
        attribs['code'] = code

        if stop.number is not None:
            attribs['number'] = str(stop.number)
        if stop.name is not None:
            attribs['name'] = stop.name
        if stop.location is not None:
            attribs['latitude'] = str(stop.location.latitude)
            attribs['longitude'] = str(stop.location.longitude)
        if stop.requestedAddress is not None:
            attribs['searchLocation'] = stop.requestedAddress

        if isinstance(stop, BusStopMashup.Stop):
            attribs['type'] = 'stop'
        elif isinstance(stop, BusStopMashup.Station):
            attribs['type'] = 'station'
        else:
            continue

        stopel = ElementTree.SubElement(root, "stop", attribs)
        stopel.tail = '\n'

        if isinstance(stop, BusStopMashup.Stop):
            routeroot = ElementTree.SubElement(stopel, "routes")
            for route in stop.routes:
                ElementTree.SubElement(routeroot, "route",
                                       { 'number': str(route.number),
                                         'direction': str(route.direction) })

    tree = ElementTree.ElementTree(root)
    tree.write(f, "utf-8")

def unxmlifyStops(f):
    stops = {}

    etree = ElementTree.parse(f)
    for stopel in etree.findall("stop"):
        type = stopel.get("type")
        if type == "station":
            stop = BusStopMashup.Station()
        elif type == "stop":
            stop = BusStopMashup.Stop()

        stop.code = stopel.get("code")
        if stop.code is None:
            continue

        stop.number = stopel.get("number")
        stop.name = stopel.get("name")

        lat = stopel.get("latitude")
        lng = stopel.get("longitude")
        if lat and lng:
            stop.location = BusStopMashup.StopLocation(lat, lng)

        stop.requestedAddress = stopel.get("searchLocation")

        if isinstance(stop, BusStopMashup.Stop):
            for routeel in stopel.find("routes").findall("route"):
                stop.routes.append(
                    BusStopMashup.StopRoute(
                             routeel.get("number"), routeel.get("direction")))

        # For consistency, make all strings unicode if they aren't already.
        if stop.code:
            stop.code = unicode(stop.code)
        if stop.name:
            stop.name = unicode(stop.name)

        stops[stop.code] = stop

    return stops


def xmlifySearched(data, f):
    root = ElementTree.Element("searched")
    root.tail = '\n'
    for sa in data:
        if sa.location is None:
            attribs = { "invalid": "1" }
        else:
            attribs = { "latitude": str(sa.location.latitude),
                        "longitude": str(sa.location.longitude) }

        if sa.address is not None:
            attribs['address'] = sa.address

        if sa.referrer is not None:
            attribs['referrer'] = str(sa.referrer)

        sel = ElementTree.SubElement(root, "address", attribs)
        sel.tail = '\n'

    tree = ElementTree.ElementTree(root)
    tree.write(f, "utf-8")

def unxmlifySearched(f):
    searched = []

    etree = ElementTree.parse(f)
    for searchel in etree.findall("address"):
        sa = SearchedAddress(searchel.get("referrer"), searchel.get("address"))

        lat = searchel.get("latitude")
        lng = searchel.get("longitude")
        if lat and lng:
            sa.location = BusStopMashup.StopLocation(lat, lng)
        searched.append(sa)

    return searched

SEARCHADDR_XML = "out/allsearched.xml"
STOPS_XML = "out/allstops.xml"

if __name__ == '__main__':
    sys.exit(main(sys.argv))
