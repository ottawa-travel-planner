#!/bin/sh

grep 'stop code=' "$@" | sed 's,^.stop code="\([^"]*\)".*$,\1,' | sort
