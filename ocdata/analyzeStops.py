#!/usr/bin/python
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

"""Thinky thinky"""

import pickle

import sys
sys.path.append("..")

import BusStopMashup
from LatLongTools import *

def analyze(stoplist):
    home = [stop for stop in stoplist
                if isinstance(stop, BusStopMashup.HomeLocation)][0]

    westiest = min(stop.location.longitude for stop in stoplist)
    eastiest = max(stop.location.longitude for stop in stoplist)
    northiest = min(stop.location.latitude for stop in stoplist)
    southiest = max(stop.location.latitude for stop in stoplist)

    print "\twest-east distance:   %f" % (eastiest - westiest)
    print "\twest-east distance:   %f km" % longdist(home.location.latitude,
                                                     eastiest, westiest)
    print "\tnorth-south distance: %f" % (southiest - northiest)
    print "\tnorth-south distance: %f km" % latdist(southiest, northiest)

    maxdist = 0.0
    for stop in stoplist:
        dist = locdist(stop.location, home.location)
        if dist > maxdist:
            maxdist = dist

    print "\tMax dist from home:   %f km" % maxdist

    print "\tDegree dist from home: %f N, %f S, %f W, %f E" % (
            home.location.latitude - northiest,
            southiest - home.location.latitude,
            home.location.longitude - westiest,
            eastiest - home.location.longitude)

if __name__ == '__main__':
    filename = "../grabs/stoplocations.pickle"
    try:
        datapoints = pickle.load(open(filename, "r"))
    except IOError, e:
        c = BusStopMashup.Client()
        datapoints = {}
        for k in ('41 antares', 'heron & data centre', 'kent & albert',
                  'carling & preston', 'bank & florence'):
            datapoints[k] = list(c.findStops(k))

        f = open(filename, "w")
        pickle.dump(datapoints, f)
        f.close()
    for k in datapoints:
        print k
        analyze(datapoints[k])
