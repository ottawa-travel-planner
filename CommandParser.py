#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

"""Splits a command into source, destination, and time."""

import re
import time

import PlanTime
import PlanLocation
import LandmarkMatcher

class Command:
    def __init__(self):
        self.start = None
        self.end = None
        self.time = None

class CommandParseException(Exception):
    pass

class CommandParser:
    def __init__(self, str):
        self.cmd = None
        self.landmarkMatcher = None

        self.parse(str)

    def parse(self, str):
        # Look for start and end at the start of the string.
        match = _full_rx.match(str)
        if not match:
            raise CommandParseException("Failed to extract source and "
                                        "destination from command '%s'" % str);
        self.cmd = Command()
        self.cmd.start = self.parseLocation(match.group("start"))
        self.cmd.end = self.parseLocation(match.group("end"))

        # Now look for time constraints.
        rule = match.group("rule")
        timestr = match.group("time")
        if rule and timestr:
            ruleMap = {
                "by": PlanTime.MUST_ARRIVE_BEFORE,
                "at": PlanTime.MUST_LEAVE_AFTER,
            }
            self.cmd.time                   \
                = PlanTime.PlanTime(self.decodeTime(timestr), ruleMap[rule])

    def parseLocation(self, str):
        match = _intersection_rx.match(str)
        if match:
            return PlanLocation.IntersectionLocation(match.group(1),
                                                     match.group(2))

        match = _stop_rx.match(str)
        if match:
            return PlanLocation.StopLocation(match.group(1))

        match = _address_rx.match(str)
        if match:
            return PlanLocation.AddressLocation(str)

        # Try a landmark.
        if self.landmarkMatcher is None:
            self.landmarkMatcher = LandmarkMatcher.LandmarkMatcher()
        return PlanLocation.LandmarkLocation(self.landmarkMatcher.match(str))

    def decodeTime(self, str):
        match = _time_rx.match(str)
        if not match:
            raise CommandParseException("Unable to decode timespec '%s'" % str)

        hour = int(match.group("hour"))

        min = 0
        minstr = match.group("min")
        if minstr is not None:
            min = int(minstr)

        mod = match.group("mod")

        if hour < 0 or hour > 23 or min < 0 or min > 59:
            raise CommandParseException("Invalid numbers in time '%s'" % str)

        if mod is not None and hour < 12 and mod.lower() == "pm":
            hour += 12

        t = list(time.localtime())
        t[3] = hour
        t[4] = min
        return time.mktime(t)

_full_re = ("(?i)^\s*(?P<start>.*?)\s+to\s+(?P<end>.*?)\s*"
            # at this point the pattern either ends, or there's a timespec
            # Require the time to have at least one digit so people can still
            # use "at" for intersections (although "and" is better)
            "(?:$|(?P<rule>by|at)\s+(?P<time>\d.*?)\s*$)")
_full_rx = re.compile(_full_re)

_intersection_re = "(?i)^(.+?)\s+(?:at|and)\s*(.+?)$"
_intersection_rx = re.compile(_intersection_re)

# "stop 6037" or just "6037"
_stop_re = "(?i)^(?:stop)?\s*(\d{4})$"
_stop_rx = re.compile(_stop_re)

# "123 some street"
_address_re = '(?i)^\d+\s+\S.*'
_address_rx = re.compile(_address_re)

# 11:30
# 11:30 pm
# 1130 pm
# 130 pm
# 130
_time_re = ("^(?P<hour>\d{1,2}):?(?P<min>\d\d)?\s*(?i)(?P<mod>am?|pm?)?$")
_time_rx = re.compile(_time_re)
