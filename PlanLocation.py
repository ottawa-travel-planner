#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

class PlanLocation:
    """Defines a trip endpoint (street address, intersection, etc.)"""

    def getPage(self, isFrom):
        """Returns a string, e.g. FromAddress.oci"""
        return self.selectPage(isFrom, self.type)

    def toPlannerParams(self, isFrom):
        """Returns a tuple of tuples of CGI parameters."""
        pass

    def selectBase(self, isFrom):
        return ("to", "from")[isFrom]

    def selectPage(self, isFrom, uctype):
        """returns e.g. FromAddress.oci"""
        return "%s%s.oci" % (self.selectBase(isFrom).title(), uctype)

    type = None

class AddressLocation(PlanLocation):
    def __init__(self, address):
        self.address = address

    def toPlannerParams(self, isFrom):
        return ((self.selectBase(isFrom) + "Address", self.address),
                ("streetType", "Other"))

    type = "Address"

class IntersectionLocation(PlanLocation):
    def __init__(self, street1, street2):
        self.street1 = street1
        self.street2 = street2

    def toPlannerParams(self, isFrom):
        base = self.selectBase(isFrom)
        return ((base + "OnStreet", self.street1),
                (base + "AtStreet", self.street2))

    type = "Intersection"

class StopLocation(PlanLocation):
    """A 560-1000 stop number."""
    def __init__(self, stop):
        self.stop = stop

    def toPlannerParams(self, isFrom):
        return ((self.selectBase(isFrom) + "560Code", self.stop),)

    # There's a "To560.oci" followed by a "To560Confirm.oci"; we can
    # skip the first one.
    type = "560Confirm"

class LandmarkLocation(PlanLocation):
    """A well-known location."""
    def __init__(self, landmark):
        self.landmark = landmark

    def toPlannerParams(self, isFrom):
        return (("landmarkAddress", self.landmark),)

    type = "LandmarkMultipleMatch"
