#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

"""A frontend for the OC Transpo Travel Planner."""

import cookielib
import urllib2
import urllib
import re

import Itinerary
from PlannerExceptions import *

def plan(start, end, time):
    """Plans a route between two Locations at a certain PlanTime."""
    planner = TravelPlannerClient()
    planner.feedStartLocation(start)
    planner.feedEndLocation(end)
    html = planner.feedTime(time)
    return Itinerary.Itinerary(start, end, time, html)

class TravelPlannerClient:
    def __init__(self):
        # Set up a cookie-aware client.
        self.cj = cookielib.CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cj))

        # Initialize the session.
        r = self._sendRequest(self.START_PAGE, None)

    def feedStartLocation(self, loc):
        self._sendRequest(loc.getPage(True), loc.toPlannerParams(True))

    def feedEndLocation(self, loc):
        self._sendRequest(loc.getPage(False), loc.toPlannerParams(False))

    def feedTime(self, time):
        # name="tp_time" action="SelectTime.oci"
        params = time.toPlannerParams()
        r = self._sendRequest("SelectTime.oci", params)

        return r

    def _sendRequest(self, page, params):
        url = self.URL_BASE + page
        if params is not None:
            url += "?" + urllib.urlencode(params)

        response = self.opener.open(url)
        self._checkObviousBadness(response)

        html = self._grabLimitedResponse(response)
        self._scanForError(html)
        return html

    def _checkObviousBadness(self, response):
        if response.code != 200:
            raise TravelPlannerException("Got HTTP " + response.code
                                         + " error from server")
        if "errorPage.oci" in response.geturl():
            # try obtaining a specific error string
            html = self._grabLimitedResponse(response)
            self._scanForError(html)

            # otherwise, throw a generic one
            raise TravelPlannerException("Redirected to error page")

    def _grabLimitedResponse(self, response):
        count = 0
        
        accum = ""
        for line in response:
            count += len(line)
            if (count <= self.RESPONSE_SIZE_LIMIT):
                accum += line
            else:
                break
        return accum



    # Regular expressions: probably the worst way to parse HTML
    # Probably the best thing to put in your breakfast cereal.

    def _scanForError(self, text):
        match = _error_rx.search(text)
        if match:
            raise TravelPlannerException(match.group("msg"))

    URL_BASE = "http://www.octranspo.com/tps/jnot/"
    START_PAGE = "startEN.oci"
    RESPONSE_SIZE_LIMIT = 100000



# Scan for an error.
# <table cellpadding="0" cellspacing="0" summary="Warning message" class="warning" width="85%">
#   <tr>
#       <td><img src="tripPlanning/images/imgWarning.gif"></td>
#       <td>The address you specified was not found.  Please enter another.</td>
#   </tr>
# </table>
_error_re = ('<table[^>]*class="(?:warning|error)[^>]*>\s*'
                '<tr>(?:\s*<td>\s*<img[^>]*>\s*</td>)?'
                    # ?s: DOTALL: . matches \n
                    # *? is non-greedy
                    '\s*<td>(?s)\s*(?P<msg>[\d\D]*?)\s*</td>')
_error_rx = re.compile(_error_re)
