#!/usr/bin/python
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

import sys

import Planner
import PlanTime
import time
import CommandParser
import ShortFormatter

def main(argv=None):
    if argv is None:
        argv = sys.argv

    cmdstr = " ".join(argv[1:])
    cmd = CommandParser.CommandParser(cmdstr).cmd

    # Leave in 3 minutes if time is unspecified.
    if cmd.time is None:
        cmd.time = PlanTime.PlanTime(time.time() + 180,
                                     PlanTime.MUST_LEAVE_AFTER)

    itin = Planner.plan(cmd.start, cmd.end, cmd.time)
    sf = ShortFormatter.ShortFormatter(itin.entries)

    for line in sf.lines:
        print line

    if itin.anyUnparsed():
        print "Warning: This itinerary display may be incomplete! Dumping..."
        print itin.entries

    return 0

if __name__ == '__main__':
    sys.exit(main())
