#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

class Rectangle(object):
    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.width = w
        self.height = h

    def __eq__(self, other):
        if self is other:
            return True
        if not isinstance(other, Rectangle):
            return False
        return (self.x == other.x and self.y == other.y
                and self.width == other.width and self.height == other.height)

    def __str__(self):
        return "(%d,%d) [%dx%d]" % (self.x, self.y, self.width, self.height)

    def __repr__(self):
        return "Rectangle(%d,%d, %d,%d)" % (
                self.x, self.y, self.width, self.height)


class PolyRect(object):
    def __init__(self, rects):
        self.rects = rects

    def area(self):
        return sum([a.width * a.height for a in self.rects])

    def subtract(self, r):
        """Subtracts one rectangle from the list."""
        newrects = []

        for i in self.rects:
            inter = intersection(i, r)
            if inter is None:
                newrects.append(i)
            else:
                # Create one rectangle above, one below, one left, one right.
                if i.y < inter.y:
                    newrects.append(Rectangle(i.x, i.y, i.width, inter.y - i.y))
                if i.y + i.height > inter.y + inter.height:
                    y = inter.y + inter.height
                    newrects.append(Rectangle(i.x, y,
                                              i.width, i.height - (y - i.y)))
                if i.x < inter.x:
                    x = i.x
                    y = max(i.y, inter.y)
                    width = inter.x - i.x
                    height = min(i.y + i.height, inter.y + inter.height) - y
                    newrects.append(Rectangle(x, y, width, height))

                if i.x + i.width > inter.x + inter.width:
                    x = inter.x + inter.width
                    y = max(i.y, inter.y)
                    width = i.width - (x - i.x)
                    height = min(i.y + i.height, inter.y + inter.height) - y
                    newrects.append(Rectangle(x, y, width, height))

        return PolyRect(newrects)

def intersection(r1, r2):
    x = max(r1.x, r2.x)
    width = min(r1.width - (x - r1.x), r2.width - (x - r2.x))
    y = max(r1.y, r2.y)
    height = min(r1.height - (y - r1.y), r2.height - (y - r2.y))

    if width <= 0 or height <= 0:
        return None
    return Rectangle(x, y, width, height)
