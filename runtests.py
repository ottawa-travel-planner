#!/usr/bin/python
#
# runtests.py
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:
#
# Author:               michael
# Creation Date:        Jun 6, 2005
#
# Copyright:            Copyright (c) 2005 CBN. All rights reserved.
#

"""Runs all tests in the "tests" directory."""

import unittest
import os
import sys
import os.path

def runAllTests(dir):
    """Returns a TestResult."""

    testLoader = unittest.TestLoader()
    suite = unittest.TestSuite()

    collectTests(dir, testLoader, suite)

    testRunner = unittest.TextTestRunner(verbosity=1)
    return testRunner.run(suite)

def collectTests(dir, testLoader, suite):
    sys.path.insert(0, dir)

    for f in os.listdir(dir):
        comp = os.path.splitext(f)
        if comp[1] == ".py":
            suite.addTest(testLoader.loadTestsFromModule(__import__(comp[0])))

if __name__ == '__main__':
    result = runAllTests("tests")
    sys.exit(not result.wasSuccessful())
