#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

"""mcPlan"""

import re
from HTMLParser import HTMLParser, HTMLParseError

import Planner
from PlannerExceptions import *

class Itinerary:
    def __init__(self, start, end, time, html):
        self.start = start
        self.end = end
        self.wantTime = time
        self.html = html

        itinParser = ItineraryParser()

        # Extract the HTML chunk for the actual itinerary, and feed it to
        # the sub-parser.
        itinParser.feed(self._extractItin(html))
        itinParser.close()
        self.entries = itinParser.entries

    def anyUnparsed(self):
        for ie in self.entries:
            if ie.type == TYPE_UNKNOWN:
                return True
        return False

    def _extractItin(self, html):
        match = _itin_rx.search(html)
        if not match:
            raise ItineraryParseException("Failed to extract itinerary")
        return match.group("itin")

# Matches the entire itinerary, pulling the table into group "itin"
_itin_re = ('<!-- begin graphical itin.*?>\s*'
                # ?s: DOTALL: . matches \n
                '(?s)(?P<itin><table.*</table>)\s*'
                '<!-- end graphical itin')
_itin_rx = re.compile(_itin_re)



class ItineraryParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.state = STATE_NOTHING

        # List of ItinEntry
        self.entries = []

        # Most recent ItinEntry, to which a bus stop number may be added
        self.lastIE = None

    def handle_starttag(self, tag, attrs):
        # The directions are contained in <td class="itinText...">.
        if tag == "td" and attrStarts(attrs, "class", "itinText"):

            # start saving text
            self.data = ""
            self.state = STATE_IN_ITIN_TEXT

        # The stop numbers are inside a block like this:
        # <span class="itinBusStop">O-TRAIN CARLETON S.</span>
        # <span class="itinBusStop"><a href="#" onClick="MM_openBrWindow('get.stop.timetable.oci?sptDate=2005-06-05&stop560=3062&stopLabel=CG995&stopName=O-TRAIN%2520CARLETON%2520S.','_stopWindow','scrollbars=yes,resizable=yes,width=800,height=600')">(3062)</a></span>
        #
        # We'll save the data in the <a> tag.
        elif self.state == STATE_AFTER_ITIN_TEXT            \
             and tag == "span" and attrStarts(attrs, "class", "itinBusStop"):

            self.data = ""
            self.state = STATE_IN_BUS_STOP

        elif self.state == STATE_IN_ITIN_TEXT and tag == "br":
            self.data += "\n"

    def handle_endtag(self, tag):
        if self.state == STATE_IN_ITIN_TEXT and tag == "td":

            self.state = STATE_AFTER_ITIN_TEXT
            self._saveTextEntry()

        elif self.state == STATE_IN_BUS_STOP and tag == "span":
            if self._saveBusStop():
                self.state = STATE_NOTHING
            else:
                # Usually there are two itinBusStop spans in a row, and
                # this was probably the first one.
                self.state = STATE_AFTER_ITIN_TEXT

    def handle_data(self, data):
        if self.state in (STATE_IN_ITIN_TEXT, STATE_IN_BUS_STOP):
            self.data += data

    def _saveTextEntry(self):
        ie = self._buildItinEntry(self.data.strip())
        self.entries.append(ie)
        self.lastIE = ie

    def _saveBusStop(self):
        match = _bus_stop_rx.search(self.data)
        if match:
            self.lastIE.busStop = match.group("stopnum")
        return bool(match)

    def _buildItinEntry(self, text):
        ie = ItinEntry(text)

        # Match each regexp in turn.
        matchTypes = (
            (TYPE_DEPART_TIME, _depart_rx),
            (TYPE_WAIT, _wait_rx),
            (TYPE_WALK_TO_STOP, _walk_to_stop_rx),
            (TYPE_WALK_TO_TRANSFER, _walk_transfer_stop_rx),
            (TYPE_TAKE_BUS, _take_bus_rx),
            (TYPE_WALK_TO_DEST, _walk_to_dest_rx),
        )
        for entry in matchTypes:
            match = entry[1].search(text)
            if match:
                ie.type = entry[0]

                # Extract fields, if any.
                groups = match.groupdict()
                for grp in groups.iteritems():
                    setattr(ie, grp[0], grp[1])

                break
        return ie

# ItineraryParser is an FSM.
STATE_NOTHING = 0
STATE_IN_ITIN_TEXT = 1
STATE_AFTER_ITIN_TEXT = 2
STATE_IN_BUS_STOP = 3


def attrStarts(attrs, name, val):
    """Searches for attribute "name" in the attrs list. If it finds it,
       returns true if its value starts with "val". Case insensitive."""

    name = name.lower()
    val = val.lower()

    for kv in attrs:
        # The attribute name is already lowercased by HTMLParser;
        # make the value lowercase, too.
        if kv[0] == name and kv[1].lower().startswith(val):
            return True
    return False

_bus_stop_re = '\((?P<stopnum>\d{4})\)'
_bus_stop_rx = re.compile(_bus_stop_re)


# Expressions we'll use to narrow an ItinEntry down to a specific type.
# We'll also pick out data fields where possible.
_ie_time_re = '[\d: APM]+'

_depart_re = '^Depart at (?P<startTime>' + _ie_time_re + ')$'
_depart_rx = re.compile(_depart_re)

_walk_to_stop_re = ('^At (?P<startTime>' + _ie_time_re
                    + '),\s*walk to (?:stop|station)\s*(?P<destination>.*?)\s*'
                      '\((?P<duration>[\d]+)\s*min')
_walk_to_stop_rx = re.compile(_walk_to_stop_re)

# If the trip includes a transfer through a nearby stop, you'll get this.
_walk_transfer_stop_re = ('(?i)^Walk to (?:stop|station)\s*'
                            '(?P<destination>.*?)\s*'
                            'for transfer\.$')
_walk_transfer_stop_rx = re.compile(_walk_transfer_stop_re)

# Flag 's' (DOTALL) is set on this one so we can skip over newlines in
# the description after "get off at stop <destination>". This description
# could be of the form "1 station(s) further" or
# "street PRESTON following street GLADSTONE.\nLast intersections: ... AVE."
_take_bus_re = ('(?is)^At (?P<startTime>' + _ie_time_re
                + '),\s*take (?:train|bus)\s+route\s+(?P<route>.*?)\s*direction'
                  '\s*(?P<direction>.*?)\s*and get off at (?:stop|station)\s*'
                  '(?P<destination>.*?)(?:\s*, .*)?\.\s*Arrive at'
                  '\s*(?P<endTime>' + _ie_time_re + ')\.')
_take_bus_rx = re.compile(_take_bus_re)

_wait_re = ('(?i)^Wait (?P<duration>\d+) min')
_wait_rx = re.compile(_wait_re)

# If Chris asks to go from 916 to 918 meadowlands, it returns one step that
# begins with "At 1:45 PM, ...". Otherwise, the "At ... " clause isn't there.
_walk_to_dest_re = ('(?i)^(?:At (?P<startTime>' + _ie_time_re + '),\s*)?'
                        'Walk to (?P<destination>.*)\.\s+Arrive at'
                        '\s*(?P<endTime>' + _ie_time_re + ')\s+'
                        '\((?P<duration>\d+) min')
_walk_to_dest_rx = re.compile(_walk_to_dest_re)

class ItinEntry:
    """One step in the plan.

       Fields:

       text: Original text from the planner.
       duration: Duration of the step in minutes.
       startTime: Start time as a string.
       endTime: End time as a string.
       route: Bus or train route.
       direction: Bus or train direction.
       destination: A string. Usually a bus stop at the end of a step.
       busStop: Bus stop number associated with the destination."""

    def __init__(self, text):
        self.text = text
        self.busStop = None
        self.type = TYPE_UNKNOWN
        self.duration = None
        self.startTime = None
        self.endTime = None
        self.route = None
        self.direction = None
        self.destination = None

    def __str__(self):
        if self.busStop:
            return "<T%d %s (%s)>" % (self.type, self.text, self.busStop)
        else:
            return "<T%d %s>" % (self.type, self.text)

    def __repr__(self):
        return self.__str__()

# ItinEntry types.
TYPE_UNKNOWN = 0
TYPE_DEPART_TIME = 1
TYPE_WALK_TO_STOP = 2
TYPE_TAKE_BUS = 3
TYPE_WAIT = 4
TYPE_WALK_TO_DEST = 5
TYPE_WALK_TO_TRANSFER = 6
