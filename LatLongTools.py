#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

import math

def latdist(lat1, lat2):
    return (lat1 - lat2) * 111.325

def longdist(lat, long1, long2):
    return (long1 - long2) * math.cos(lat / 180.0 * math.pi) * 111.325

def locdist(loc1, loc2):
    """terrible approximation"""
    xdist = longdist(loc1.latitude, loc1.longitude, loc2.longitude)
    ydist = latdist(loc1.latitude, loc2.latitude)
    return math.sqrt(xdist * xdist + ydist * ydist)

def findCorners(center, entries):
    """Takes a list of two-element tuples: (location, tag).
       Returns a four-element list, one for each corner (northwest,
       northeast, southwest, southeast). Each element is in turn a list of
       tags, sorted by descending distance from the center."""

    # Split by nearest corner
    splitEntries = [[], [], [], []]
    for entry in entries:
        if entry[0].latitude >= center.latitude:
            if entry[0].longitude <= center.longitude:
                splitEntries[NORTHWEST].append(entry)
            else:
                splitEntries[NORTHEAST].append(entry)
        else:
            if entry[0].longitude <= center.longitude:
                splitEntries[SOUTHWEST].append(entry)
            else:
                splitEntries[SOUTHEAST].append(entry)

    for corner in splitEntries:
        # Descending sort by distance from center.
        corner.sort(None, lambda entry: locdist(entry[0], center), True)

    # Return the tags.
    return [[entry[1] for entry in corner] for corner in splitEntries]

NORTHWEST = 0
NORTHEAST = 1
SOUTHWEST = 2
SOUTHEAST = 3
