#
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

"""Formats an Itinerary for terse display."""

import re

import Itinerary

class ShortFormatter:
    def __init__(self, itinentries):
        self.entries = itinentries

        # A list of text strings to display.
        self.lines = []

        self.format()

    def format(self):
        for ie in self.entries:
            if ie.type == Itinerary.TYPE_WALK_TO_STOP:
                self.formatWalkToStop(ie)
            elif ie.type == Itinerary.TYPE_TAKE_BUS:
                self.formatTakeBus(ie)
            elif ie.type == Itinerary.TYPE_WALK_TO_TRANSFER:
                self.formatWalkToTransfer(ie)
            elif ie.type == Itinerary.TYPE_WALK_TO_DEST:
                self.formatWalkToDest(ie)

    def formatWalkToStop(self, ie):
        line = "-%s walk %s (%s)"                \
                % (self.canonTime(ie.startTime),
                   self.canonDest(ie.destination), ie.busStop)
        self.lines.append(line)

    def formatTakeBus(self, ie):
        line = "-%s %s %s to %s at %s"              \
                % (self.canonTime(ie.startTime), ie.route,
                   self.canonDirection(ie.direction),
                   self.canonDest(ie.destination),
                   self.canonTime(ie.endTime))
        self.lines.append(line)

    def formatWalkToTransfer(self, ie):
        line = "-walk %s (%s)" % (self.canonDest(ie.destination), ie.busStop)
        self.lines.append(line)

    def formatWalkToDest(self, ie):
        line = "-walk %s %s"                            \
                % (self.canonDest(ie.destination), self.canonTime(ie.endTime))
        self.lines.append(line)

    def canonDest(self, d):
        """MERIVALE / COLONNADE  ->  Merivale/Colonnade"""
        match = _canon_dest_rx.match(d)
        if match:
            d = "/".join((match.group(1), match.group(2)))

        # Hurdman Stop/Arret 2A -> Hurdman 2A
        d = _arret_rx.sub("", d)
        return d.title()

    def canonTime(self, t):
        """11:05 AM  ->   11:05"""
        return t.split()[0]

    def canonDirection(self, d):
        """St Laurent   ->   St Lau. Take the first 3 letters of all words
           after the first."""
        words = d.split()
        canonwords = [words[0]]
        for w in words[1:]:
            canonwords.append(w[0:3])   # ok even if only 2 letters in w
        return " ".join(canonwords)

    def __str__(self):
        return "\n".join(self.lines)

    def __repr__(self):
        print self.__str__()

_canon_dest_re = "^(.*)\s/\s+(.*)$"
_canon_dest_rx = re.compile(_canon_dest_re)

_arret_re = r"(?i)\s*Stop/Arr..\b"
_arret_rx = re.compile(_arret_re)
