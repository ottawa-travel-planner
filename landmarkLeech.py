#!/usr/bin/python
# vi: set softtabstop=4 shiftwidth=4 tabstop=8 expandtab:

"""Downloads a list of landmarks from the server."""

import re
import sys
import urllib

import Planner

class LandmarkLeech(Planner.TravelPlannerClient):

    def leech(self, fp):
        html = self._sendRequest("FromLandmarkSetup.oci", None)
        for match in _category_rx.finditer(html):
            self.grabCategory(fp, match.group("code"), match.group("desc"))

    def grabCategory(self, fp, code, desc):
        # the description is just cosmetic - it gets displayed at the
        # top of the FromLandmarkType page.
        params = (("landmarkType", code), ("landmarkDesc", desc))
        html = self._sendRequest("FromLandmarkType.oci", params)
        for landmark in _landmark_rx.findall(html):
            print >> fp, "|".join((code, urllib.unquote(landmark)))

_category_re = '(?i)<option\s+value="(?P<code>.*?)">(?P<desc>.*?)</option>'
_category_rx = re.compile(_category_re)

_landmark_re = ('(?i)<a href="FromLandmarkMultipleMatch.oci\?'
                'landmarkAddress=(.*)">')
_landmark_rx = re.compile(_landmark_re)

def main(argv=None):
    if argv is None:
        argv = sys.argv

    l = LandmarkLeech()
    l.leech(sys.stdout)

    return 0

if __name__ == '__main__':
    sys.exit(main())
